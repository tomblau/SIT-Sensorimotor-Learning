
import time
import numpy as np
from rllab.misc import special
from rllab.misc import tensor_utils
from rllab.algos import util
import rllab.misc.logger as logger


class Sampler(object):
    def start_worker(self):
        """
        Initialize the sampler, e.g. launching parallel workers if necessary.
        """
        raise NotImplementedError

    def obtain_samples(self, itr):
        """
        Collect samples for the given iteration number.
        :param itr: Iteration number.
        :return: A list of paths.
        """
        raise NotImplementedError

    def process_samples(self, itr, paths):
        """
        Return processed sample data (typically a dictionary of concatenated tensors) based on the collected paths.
        :param itr: Iteration number.
        :param paths: A list of collected paths.
        :return: Processed sample data.
        """
        raise NotImplementedError

    def shutdown_worker(self):
        """
        Terminate workers if necessary.
        """
        raise NotImplementedError


class BaseSampler(Sampler):
    def __init__(self, algo, subsample_factor=1):
        """
        :type algo: BatchPolopt
        """
        self.algo = algo
        self.subsample_factor = subsample_factor

    def process_samples(self, itr, paths):
        baselines = []
        returns = []


        if hasattr(self.algo.baseline, "predict_n"):
            all_path_baselines = self.algo.baseline.predict_n(paths)
        else:
            all_path_baselines = [self.algo.baseline.predict(path) for path in paths]

        for idx, path in enumerate(paths):
            path_baselines = np.append(all_path_baselines[idx], 0)
            deltas = path["rewards"] + \
                     self.algo.discount * path_baselines[1:] - \
                     path_baselines[:-1]
            path["advantages"] = special.discount_cumsum(
                deltas, self.algo.discount * self.algo.gae_lambda)
            path["returns"] = special.discount_cumsum(path["rewards"], self.algo.discount)
            baselines.append(path_baselines[:-1])
            returns.append(path["returns"])

        ev = special.explained_variance_1d(
            np.concatenate(baselines),
            np.concatenate(returns)
        )
        indices = np.arange( np.sum( [len(path['rewards']) for path in paths] ) )

        if not self.algo.policy.recurrent:
            observations = tensor_utils.concat_tensor_list([path["observations"] for path in paths])
            actions = tensor_utils.concat_tensor_list([path["actions"] for path in paths])
            rewards = tensor_utils.concat_tensor_list([path["rewards"] for path in paths])
            returns = tensor_utils.concat_tensor_list([path["returns"] for path in paths])
            advantages = tensor_utils.concat_tensor_list([path["advantages"] for path in paths])
            env_infos = tensor_utils.concat_tensor_dict_list([path["env_infos"] for path in paths])
            agent_infos = tensor_utils.concat_tensor_dict_list([path["agent_infos"] for path in paths])

            if self.subsample_factor < 1:
                if 'indices' in paths[0]:
                    indices = paths[0]['indices']
                else:
                    indices = np.random.choice(
                        rewards.size, int(rewards.size * self.subsample_factor), replace=False)
            indices.sort()

            save = 0
            if save:
                ts = int(1e5*time.time())
                fp = "/rllab/rllab/envs/vrep/data/baselines/demo_"
                np.savez_compressed("%sobs_%d" %(fp, ts), observations)
                #np.savez_compressed("%sact_%d" %(fp, ts), actions)
                #np.savez_compressed("%sret_%d" %(fp, ts), returns)
            if self.algo.center_adv:
                advantages = util.center_advantages(advantages)

            if self.algo.positive_adv:
                advantages = util.shift_advantages_to_positive(advantages)

            average_discounted_return = \
                np.mean([path["returns"][0] for path in paths])

            if "intr_rewards" in paths[0]: 
                undiscounted_returns = [sum(path["rewards"] - path['intr_rewards']) for path in paths]
            else:
                undiscounted_returns = [sum(path["rewards"]) for path in paths]

            successes = sum([path["success"] for path in paths])

            ent = np.mean(self.algo.policy.distribution.entropy(agent_infos))

            samples_data = dict(
                observations=observations[indices],
                actions=actions[indices],
                rewards=rewards[indices],
                returns=returns[indices],
                advantages=advantages[indices],
                env_infos=dict([(k, env_infos[k][indices]) for k in env_infos.keys()]),
                agent_infos=dict([(k, agent_infos[k][indices]) for k in agent_infos.keys()]),
                paths=paths,
            )
        else:
            max_path_length = max([len(path["advantages"]) for path in paths])

            # make all paths the same length (pad extra advantages with 0)
            obs = [path["observations"] for path in paths]
            obs = tensor_utils.pad_tensor_n(obs, max_path_length)

            if self.algo.center_adv:
                raw_adv = np.concatenate([path["advantages"] for path in paths])
                adv_mean = np.mean(raw_adv)
                adv_std = np.std(raw_adv) + 1e-8
                adv = [(path["advantages"] - adv_mean) / adv_std for path in paths]
            else:
                adv = [path["advantages"] for path in paths]

            adv = np.asarray([tensor_utils.pad_tensor(a, max_path_length) for a in adv])

            actions = [path["actions"] for path in paths]
            actions = tensor_utils.pad_tensor_n(actions, max_path_length)

            rewards = [path["rewards"] for path in paths]
            rewards = tensor_utils.pad_tensor_n(rewards, max_path_length)

            returns = [path["returns"] for path in paths]
            returns = tensor_utils.pad_tensor_n(returns, max_path_length)

            agent_infos = [path["agent_infos"] for path in paths]
            agent_infos = tensor_utils.stack_tensor_dict_list(
                [tensor_utils.pad_tensor_dict(p, max_path_length) for p in agent_infos]
            )

            env_infos = [path["env_infos"] for path in paths]
            env_infos = tensor_utils.stack_tensor_dict_list(
                [tensor_utils.pad_tensor_dict(p, max_path_length) for p in env_infos]
            )

            valids = [np.ones_like(path["returns"]) for path in paths]
            valids = tensor_utils.pad_tensor_n(valids, max_path_length)

            average_discounted_return = \
                np.mean([path["returns"][0] for path in paths])

            if "intr_rewards" in paths[0]: 
                undiscounted_returns = [sum(path["rewards"] - path['intr_rewards']) for path in paths]
            else:
                undiscounted_returns = [sum(path["rewards"]) for path in paths]

            successes = sum(
                [path["rewards"][-1] - path["env_infos"]["entropy"][-1] >= 0 for path in paths]
                )

            ent = np.sum(self.algo.policy.distribution.entropy(agent_infos) * valids) / np.sum(valids)

            samples_data = dict(
                observations=obs,
                actions=actions,
                advantages=adv,
                rewards=rewards,
                returns=returns,
                valids=valids,
                agent_infos=agent_infos,
                env_infos=env_infos,
                paths=paths,
            )

        if 0:
            observations = np.concatenate([p["observations"] for p in paths])
            actions = np.concatenate([p["actions"] for p in paths])
            returns = np.concatenate([p["returns"] for p in paths])
            tstamp = int(1e5*time.time())
            fpath = "/rllab/rllab/envs/data/mountaincar"
            np.savez_compressed("%s/demo_obs_%d.npz" %(fpath,tstamp), observations)
            #np.savez_compressed("%s/demo_act_%d.npz" %(fpath,tstamp), actions)
            #np.savez_compressed("%s/demo_ret_%d.npz" %(fpath,tstamp), returns)


        logger.log("fitting baseline...")
        if hasattr(self.algo.baseline, 'fit_with_samples'):
            self.algo.baseline.fit_with_samples(paths, samples_data)
        else:
            self.algo.baseline.fit(paths, indices)
        logger.log("fitted")

        if "intr_rewards" in paths[0]:
            intr_rewards = tensor_utils.concat_tensor_list([path["intr_rewards"] for path in paths])
            logger.record_tabular('MeanIntrReward', np.mean(intr_rewards))
            logger.record_tabular('StdIntrReward', np.std(intr_rewards))

        logger.record_tabular('Iteration', itr)
        logger.record_tabular('AverageDiscountedReturn',
                              average_discounted_return)
        logger.record_tabular('AverageReturn', np.mean(undiscounted_returns))
        logger.record_tabular('ExplainedVariance', ev)
        logger.record_tabular('NumTrajs', len(paths))
        logger.record_tabular('Entropy', ent)
        logger.record_tabular('Perplexity', np.exp(ent))
        logger.record_tabular('StdReturn', np.std(undiscounted_returns))
        logger.record_tabular('AllReturns', undiscounted_returns)
        logger.record_tabular('Successes', successes)
        logger.record_tabular('MaxReturn', np.max(undiscounted_returns))
        logger.record_tabular('MinReturn', np.min(undiscounted_returns))

        return samples_data

    def find_successful_paths(self, paths):
        succ_ids = [i for i in range(len(paths)) if paths[i]["rewards"][-1] > 0]
        obs = np.concatenate([paths[i]["observations"] for i in succ_ids])
        act = np.concatenate([paths[i]["actions"] for i in succ_ids])
        return dict(observations=obs, actions=act)
