import numpy as np
from rllab.misc import tensor_utils
import time


def rollout(env, agent, max_path_length=np.inf, animated=False, speedup=1, regularized=False):
    observations = []
    actions = []
    rewards = []
    agent_infos = []
    env_infos = []
    cup_pos = []
    dists = []
    d = False
    o = env.reset()
    agent.reset()
    path_length = 0
    if animated:
        env.render()
    while path_length < max_path_length:
        if isinstance(o, dict): o = o['observation']
        a, agent_info = agent.get_action(o)
        if regularized:
            next_o, r, d, env_info = env.step(a, agent_info)
        else:
            next_o, r, d, env_info = env.step(a)
        if 'action' in env_info:
            a = env_info['action']
        if 'cup_pos' in env_info:
            cup_pos.append(env_info['cup_pos'])
        if 'dist' in env_info:
            dists.append(env_info['dist'])  
        observations.append(env.observation_space.flatten(o))
        rewards.append(r)
        actions.append(env.action_space.flatten(a))
        agent_infos.append(agent_info)
        env_infos.append(env_info)
        path_length += 1
        if d:
            if 'success' in env_info:
                d = env_info['success']
            elif r < 0:
                d = False
            break
        o = next_o
        if animated:
            env.render()
            timestep = 0.05
            time.sleep(timestep / speedup)
    if animated:
        return
    if 0:
        demo_obs = np.stack(observations)[:,-6:]
        demo_act = np.stack(actions)
        cup_pos = np.stack(cup_pos)
        #demo_rewards = np.stack(rewards)
        #std = np.stack([info['log_std'] for info in agent_infos])
        #d = np.asarray(dists)
        tstamp = int(time.time() * 1e5)
        fpath = "/rllab/rllab/envs/vrep/data/lowd_half"
        np.savez_compressed("%s/demo_obs_%d.npz" %(fpath,tstamp), demo_obs)
        np.savez_compressed("%s/demo_act_%d.npz" %(fpath,tstamp), demo_act)
        np.savez_compressed("%s/cup_pos_%d.npz" %(fpath,tstamp), cup_pos)
        #np.savez_compressed("%s/rewards_%d.npz" %(fpath,tstamp), demo_rewards)
        #np.savez_compressed("%s/stds_%d.npz" %(fpath,tstamp), std)
       # np.savez_compressed("%s/dists_%d.npz" %(fpath,tstamp), d)


    return dict(
        observations=tensor_utils.stack_tensor_list(observations),
        actions=tensor_utils.stack_tensor_list(actions),
        rewards=tensor_utils.stack_tensor_list(rewards),
        agent_infos=tensor_utils.stack_tensor_dict_list(agent_infos),
        env_infos=tensor_utils.stack_tensor_dict_list(env_infos),
        success=d
    )
