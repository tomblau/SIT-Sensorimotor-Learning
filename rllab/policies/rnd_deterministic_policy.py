import lasagne
import lasagne.layers as L
import lasagne.nonlinearities as NL
import lasagne.updates as U
import numpy as np
import inspect

from rllab.spaces import Box
from rllab.core.lasagne_powered import LasagnePowered
from rllab.core.serializable import Serializable
from rllab.core.parameterized import Parameterized
from rllab.policies.base import Policy
from rllab.misc.overrides import overrides
from rllab.misc import ext
import theano.tensor as TT


class RNDDeterministicPolicy(Policy, LasagnePowered):
    def __init__(
            self,
            env_spec,
            network,
            target_network,
            predictor_network,
            eta=1.0,
    ):
        """
        :param env_spec:
        :param mean_network: custom network for the output mean
        :param std_network: custom network for the output log std
            must have same input var as mean_network
        :param target_network: custom network providing the prediction target
        :param predictor_network: custom network predicting target_network
            must have same input var as target_network
        ":param eta: co-efficient for weighting intrinsic rewards"
        :return:
        """
        Serializable.quick_init(self, locals())

        obs_dim = env_spec.observation_space.flat_dim
        action_dim = env_spec.action_space.flat_dim

        self._network = network
        l_output = network.output_layer
        self._l_out = l_output

        # Note the deterministic=True argument. It makes sure that when getting
        # actions from single observations, we do not update params in the
        # batch normalization layers
        action_var = L.get_output(l_output, deterministic=True)
        self._output_layer = l_output
        self._f_actions = ext.compile_function([network.input_var], action_var)

        super(RNDDeterministicPolicy, self).__init__(env_spec)
        LasagnePowered.__init__(self, [l_output])

        #intrinsic reward stuff
        self.eta = eta
        self._target_network = target_network
        self._predictor_network = predictor_network
        self._l_target = target_network.output_layer
        self._l_predictor = predictor_network.output_layer
        target_var = L.get_output(self._l_target, deterministic=True)
        predictor_var = L.get_output(self._l_predictor, deterministic=True)
        t_obs_var = target_network.input_layer.input_var
        delta_var = TT.mean( TT.sqr(target_var - predictor_var), axis=-1 )
        pred_params = L.get_all_params(self._l_predictor)
        updates = U.adam(delta_var.mean(), pred_params)

        self._norm_size = 0
        self._norm_mean = np.zeros(())
        self._norm_var = np.ones(())

        self._f_pred_err = ext.compile_function(
            inputs=[t_obs_var],
            outputs=delta_var,
            )
        self._f_train_pred = ext.compile_function(
            inputs=[t_obs_var],
            updates=updates,
            outputs=delta_var,
            )
    
    def get_action(self, observation):
        action = self._f_actions([observation])[0]
        return action, dict()

    def get_actions(self, observations):
        return self._f_actions(observations), dict()

    def get_action_sym(self, obs_var):
        return L.get_output(self._output_layer, obs_var, deterministic=True)

    def get_intrinsic_reward(self, obs):
        int_rew = self._f_pred_err(obs)
        norm_rew = self.normalize_reward(int_rew)
        return {'raw_int_rew': int_rew, 'norm_int_rew': self.eta * norm_rew}

    def normalize_reward(self, rew):
        return rew / np.sqrt(self._norm_var)

    def update_normalization(self, new_batch):
        """
        use Chan et al's parallel variance algorithm
        :param new_batch: np.array of new data to add
        """
        batch_size = new_batch.shape[0]
        batch_mean = new_batch.mean(axis=0)
        batch_var = new_batch.var(axis=0)
        if self._norm_size == 0:
            self._norm_size = batch_size
            self._norm_mean = batch_mean
            self._norm_var = batch_var
        else:
            delta = batch_mean - self._norm_mean
            new_size = self._norm_size + batch_size
            m_a = self._norm_var * self._norm_size
            m_b = batch_var * batch_size
            M2 = m_a + m_b + \
                np.square(delta) * self._norm_size * batch_size / new_size
            new_var = M2 / (new_size)
            new_mean = self._norm_mean + delta * batch_size / new_size

            self._norm_size = new_size
            self._norm_mean = new_mean
            self._norm_var = new_var

    def update_predictor(self, obs):
        """
        do one update of the predictor network
        :param obs: np.array of observations
        """
        return self._f_train_pred(obs)

    @property
    @overrides
    def intrinsic(self):
        return True

    def __getstate__(self):
        d = Parameterized.__getstate__(self)
        spec = inspect.getfullargspec(self.__init__)
        in_order_args = spec.args[1:]
        idx = in_order_args.index('eta')
        d["__args"] = d["__args"][:idx] + (self.eta,) + d["__args"][idx+1:]
        return d