from rllab.spaces import Box
from rllab.envs.env_spec import EnvSpec
import numpy as np

if __name__ == "__main__":
    ones = np.ones(1)
    actionspace = Box(-1*ones,1*ones)
    obspace = Box(-1*ones,1*ones)
    env = EnvSpec(obspace, actionspace)
    from rllab.policies.blr_policy import BLRPolicy
    pi = BLRPolicy(env,beta=25.)
    import joblib
    joblib.dump(dict(policy=pi), "policy.pkl")
