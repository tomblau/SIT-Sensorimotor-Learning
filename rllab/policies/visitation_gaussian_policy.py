import lasagne
import lasagne.layers as L
import lasagne.nonlinearities as NL
import numpy as np
import time
import inspect

from rllab.core.network import MLP
from rllab.spaces import Box

from rllab.core.serializable import Serializable
from rllab.core.parameterized import Parameterized
from rllab.policies.gaussian_mlp_policy import GaussianMLPPolicy
from rllab.misc.overrides import overrides
from rllab.misc import logger
from rllab.misc import ext
from rllab.distributions.diagonal_gaussian import DiagonalGaussian
import theano.tensor as TT


class VisitationGaussianPolicy(GaussianMLPPolicy):
    def __init__(
            self,
            env_spec,
            mean_network,
            std_network,
            vis_network,
            vis_regression,
            kernel,
            min_std=1e-6,
            dist_cls=DiagonalGaussian,
            eta=1.0,
    ):
        """
        :param env_spec:
        :param hidden_sizes: list of sizes for the fully-connected hidden layers
        :param learn_std: Is std trainable
        :param init_std: Initial std
        :param adaptive_std:
        :param std_share_network:
        :param std_hidden_sizes: list of sizes for the fully-connected layers for std
        :param min_std: whether to make sure that the std is at least some threshold value, to avoid numerical issues
        :param std_hidden_nonlinearity:
        :param hidden_nonlinearity: nonlinearity used for each hidden layer
        :param output_nonlinearity: nonlinearity for the output layer
        :param mean_network: custom network for the output mean
        :param std_network: custom network for the output log std
        :return:
        """
        Serializable.quick_init(self, locals())
        assert isinstance(env_spec.action_space, Box)
        super(VisitationGaussianPolicy, self).__init__(
            env_spec, mean_network=mean_network, std_network=std_network,
            min_std=min_std, dist_cls=dist_cls)

        self.eta = eta
        self._v_network = vis_network
        self._v_regression = vis_regression
        self._kernel = kernel
        
        v_l_out = vis_network.output_layer
        v_obs_var = vis_network.input_layer.input_var
        self._v_l_out = v_l_out
        v_phi = L.get_output(self._v_l_out, deterministic=True)
        v_phi = self._kernel.transform(v_phi)
        self._v_regression.update_opt(v_obs_var, v_phi)
        v_mean_var, v_std_var, v_log_std_var = self._v_regression.get_moment_vars(v_phi)

        phi_var = TT.dmatrix("phis")
        phi_mean_var, phi_std_var, phi_log_std_var = self._v_regression.get_moment_vars(phi_var)
        

        self._f_v_phis = ext.compile_function(
            inputs=[v_obs_var],
            outputs=v_phi
            )
        self._f_vis_unc = ext.compile_function(
            inputs=[v_obs_var],
            outputs=[v_log_std_var],
        )
        self._f_phi_unc = ext.compile_function(
            inputs=[phi_var],
            outputs=[phi_log_std_var],
        )


    def get_vis_phis_sym(self, obs_var, deterministic=True):
        phi= L.get_output(self._v_l_out, obs_var, deterministic=deterministic)
        return self._kernel.transform(phi)

    def get_vis_phis(self, observations):
        flat_obs = self.observation_space.flatten_n(observations)
        phis = self._f_v_phis(flat_obs).astype('float64')
        return phis

    def vis_raw_update(self, o):
        #sanity check
        if len(o.shape) == 1:
            o = o.reshape(1,o.shape[0])
        phi = self.get_phis(o)
        self._v_regression.opts['smw_s_update'](phi)

    def vis_phi_update(self, phi):
        self._v_regression.opts['smw_s_update'](phi)

    def vis_phi_batch_update(self, phi):
        """
        update the blr using input in feature space
        """
        phi = self._v_regression.add_bias(phi)
        ptp = phi.T.dot(phi)
        self._v_regression.opts['f_p_update_s_iterative'](ptp)

    def get_intrinsic_reward(self, o):
        phi = self.get_phis(o)
        unc = self._f_vis_unc(phi)
        return self.eta * unc

    def get_intrinsic_phi_reward(self, phi):
        unc = self._f_phi_unc(phi)[0][:,0]
        return self.eta * unc



    @property
    @overrides
    def intrinsic(self):
        return True

    def __getstate__(self):
        d = Parameterized.__getstate__(self)
        spec = inspect.getfullargspec(self.__init__)
        in_order_args = spec.args[1:]
        idx = in_order_args.index('eta')
        d["__args"] = d["__args"][:idx] + (self.eta,) + d["__args"][idx+1:]
        return d
