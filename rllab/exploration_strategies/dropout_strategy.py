from rllab.misc.overrides import overrides
from rllab.misc.ext import AttrDict
from rllab.core.serializable import Serializable
from rllab.spaces.box import Box
from rllab.exploration_strategies.base import ExplorationStrategy
import numpy as np
import numpy.random as nr


class DropoutStrategy(ExplorationStrategy, Serializable):
    """
    This strategy assumes the policy is a network with dropout.
    the noise is gaussian with variance estimated from stochastic forward 
    passes through the network
    """

    def __init__(self, env_spec, sample_size=10, **kwargs):
        assert isinstance(env_spec.action_space, Box)
        assert len(env_spec.action_space.shape) == 1
        Serializable.quick_init(self, locals())
        self.sample_size = sample_size
        self._action_space = env_spec.action_space

    @overrides
    def get_action(self, t, observation, policy, **kwargs):
        action, _ = policy.get_action(observation)
       # actions = [policy.get_stochastic_action(observation)[0] for _ in range(self.sample_size)]
        #sigma = np.concatenate(actions).std(axis=0)
        #print("sigma = %f" %sigma)
        #return np.clip(action + np.random.normal(size=len(action)) * sigma, self._action_space.low,
        #               self._action_space.high)
        return action
