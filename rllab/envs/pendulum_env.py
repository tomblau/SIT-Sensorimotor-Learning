import numpy as np
from numpy import pi
from rllab import spaces
import gym
import math
from rllab.envs.base import PropertyEnv
from cached_property import cached_property


class PendulumEnv(PropertyEnv):
	def __init__(self, seed=None, human=False):
		self.env = gym.envs.make("Pendulum-v0")
		self.human = human
		if self.human:
			self.viewer = self.env.env.viewer
		self.env.env.seed(seed)

	def reset(self):
		low = np.array([np.pi - 1., -1.])
		high = np.array([np.pi + 1., 1.])
		self.env.env.state = self.env.env.np_random.uniform(low=low, high=high)
		self.env.env.last_u = None
		return self.env.env._get_obs()

	def step(self,u):
		th, thdot = self.env.env.state # th := theta

		g = 10.
		m = 1.75
		l = 1.
		dt = self.env.env.dt

		u = np.clip(u, -self.env.env.max_torque, self.env.env.max_torque)[0]
		self.env.env.last_u = u # for rendering
		cos = np.cos(th)
		done = False
		if cos < 0.9:
			reward = -1.
			#done=False
		else:
			reward = cos
			#done=True

		newthdot = thdot + (-3*g/(2*l) * np.sin(th + np.pi) + 3./(m*l**2)*u) * dt
		newth = th + newthdot*dt
		newthdot = np.clip(newthdot, -self.env.env.max_speed, self.env.env.max_speed) #pylint: disable=E1111

		self.env.env.state = np.array([newth, newthdot])
		return self.env.env._get_obs(), reward, done, {'success': done}


	def render(self):
		self.env.env.render()

	def close(self):
		self.env.env.close()

	@cached_property
	def action_space(self):
		return spaces.Box(low=self.env.env.action_space.low,
						  high=self.env.env.action_space.high,
						  dtype=self.env.env.action_space.dtype)

	@cached_property
	def observation_space(self):
		return spaces.Box(low=self.env.env.observation_space.low,
						  high=self.env.env.observation_space.high,
						  dtype=self.env.env.observation_space.dtype)

	@property
	def horizon(self):
		return 100

def angle_normalize(x):
	return (((x+np.pi) % (2*np.pi)) - np.pi)
