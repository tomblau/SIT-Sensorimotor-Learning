from gym.envs.box2d.car_racing import CarRacing
import numpy as np
from rllab import spaces
import gym
from rllab.envs.base import PropertyEnv
from cached_property import cached_property

RES = 96
DOWNSIZE_RES = 84

class CarRacingEnv(PropertyEnv):
	def __init__(self, history_len=4, frame_skip=2, human=False):
		"""
		history_len - the length of observation history to maintain
		frame_skip - the distance between subsequent frames recorded in the history. By default record
			every 2nd frame
		human - enable human interaction
		"""
		self.env = gym.envs.make("CarRacing-v0")
		self.render()
		self.human = human
		if self.human:
			self.viewer = self.env._unwrapped.viewer
		self.total_reward = 0.
		self.timestep = 0
		self.history = []
		self.history_len = history_len
		self.frame_skip = frame_skip

	def reset(self):
		res = self.env.reset()
		self.render()
		self.total_reward = 0.
		self.timestep = 0
		self.history = []
		return self.process_obs(res)

	def step(self, action):
		o, r, d, info = self.env.step(action)
		if self.human: self.render()
		self.timestep += 1
		self.total_reward += r
		o = self.process_obs(o)
		info['success'] = False
		if d and self.total_reward > 900.:
			info['success'] = True
		return o, r, d, info

	def render(self):
		self.env.render()

	def process_obs(self, o):
		o = self.monochrome_obs(o)
		o = self.crop_obs(o)
		o /= 255.
		self.record_history(o)
		full_obs = np.concatenate(self.history, axis=2)
		if len(self.history) < self.history_len:
			zero_pad = np.zeros(
				(DOWNSIZE_RES, DOWNSIZE_RES, self.history_len - len(self.history))
			)
			full_obs = np.concatenate([zero_pad, full_obs], axis=2)
		return full_obs

	def monochrome_obs(self, o):
		"""
		o - a RESxRESx3 RGB image
		"""
		return np.dot(o[..., :3], [0.299, 0.587, 0.114]).reshape(RES, RES, 1)

	def crop_obs(self, o):
		"""
		o - a RESxRESx1 grayscale image
		"""
		diff = RES - DOWNSIZE_RES
		cropped =  o[:-diff, diff//2:-diff//2]
		return cropped

	def record_history(self, o):
		"""
		o - a DOWNSIZE_RESxDOWNSIZE_RESx1 grayscale image
		"""
		if self.timestep % self.frame_skip == 0:
			self.history.append(o)
		if len(self.history) > self.history_len:
			self.history.pop(0)

	def close(self):
		self.env.close()

	@cached_property
	def action_space(self):
		return spaces.Box(low=np.array([-1., 0., 0.]), high=np.array([1., 1., 1.]), dtype=np.float32)

	@cached_property
	def observation_space(self):
	    return spaces.Box(low=0., high=1.,
	    	shape=(DOWNSIZE_RES, DOWNSIZE_RES, self.history_len), dtype=np.float32)

	@property
	def horizon(self):
		return 1000