from rllab.envs.robotics.fetch_env import FetchEnv
from rllab.envs.robotics.visual_fetch_env import VisualFetchEnv
from rllab.envs.robotics.fetch.slide import FetchSlideEnv
from rllab.envs.robotics.fetch.pick_and_place import FetchPickAndPlaceEnv
from rllab.envs.robotics.fetch.push import FetchPushEnv
from rllab.envs.robotics.fetch.reach import FetchReachEnv
from rllab.envs.robotics.fetch.visual_reach import VisualFetchReachEnv

from rllab.envs.robotics.hand.reach import HandReachEnv
from rllab.envs.robotics.hand.manipulate import HandBlockEnv
from rllab.envs.robotics.hand.manipulate import HandEggEnv
from rllab.envs.robotics.hand.manipulate import HandPenEnv
