import numpy as np
from numpy import pi
from rllab import spaces
import gym
from rllab.envs.base import PropertyEnv
from cached_property import cached_property


class AcrobotEnv(PropertyEnv):
	def __init__(self, seed=None, human=False):
		"""
		history_len - the length of observation history to maintain
		frame_skip - the distance between subsequent frames recorded in the history. By default record
			every 2nd frame
		human - enable human interaction
		"""
		self.env = gym.envs.make("Acrobot-v1")
		self.env.action_space = spaces.Box(low=np.array([-1.]), high=np.array([1.]), dtype=np.float32)
		self.human = human
		if self.human:
			self.viewer = self.env._unwrapped.viewer
		self.env.env.seed(seed)

	def reset(self):
		return self.env.reset()

	def step(self, action):
		s = self.env.env.state
		torque = action

		# Add noise to the force action
		if self.env.env.torque_noise_max > 0:
			torque += self.env.env.np_random.uniform(-self.env.env.torque_noise_max, self.env.env.torque_noise_max)

		# Now, augment the state with our force action so it can be passed to
		# _dsdt
		s_augmented = np.append(s, torque)

		ns = rk4(self.env.env._dsdt, s_augmented, [0, self.env.env.dt])
		# only care about final timestep of integration returned by integrator
		ns = ns[-1]
		ns = ns[:4]  # omit action
		# ODEINT IS TOO SLOW!
		# ns_continuous = integrate.odeint(self.env.env._dsdt, self.env.env.s_continuous, [0, self.env.env.dt])
		# self.env.env.s_continuous = ns_continuous[-1] # We only care about the state
		# at the ''final timestep'', self.env.env.dt

		ns[0] = wrap(ns[0], -pi, pi)
		ns[1] = wrap(ns[1], -pi, pi)
		ns[2] = bound(ns[2], -self.env.env.MAX_VEL_1, self.env.env.MAX_VEL_1)
		ns[3] = bound(ns[3], -self.env.env.MAX_VEL_2, self.env.env.MAX_VEL_2)
		self.env.env.state = ns
		terminal = self._terminal()
		reward = -1. if not terminal else 0.
		return self.env.env._get_ob(), reward, terminal, {'success' : terminal}

	def _terminal(self):
		s = self.env.env.state
		return bool(-np.cos(s[0]) - np.cos(s[1] + s[0]) > 1.9)

	def render(self):
		self.env.render()

	def close(self):
		self.env.close()

	@cached_property
	def action_space(self):
		return self.env.action_space

	@cached_property
	def observation_space(self):
		return spaces.Box(low=self.env.observation_space.low,
						  high=self.env.observation_space.high,
						  dtype=self.env.observation_space.low.dtype)

	@property
	def horizon(self):
		return 500

def wrap(x, m, M):
    """
    :param x: a scalar
    :param m: minimum possible value in range
    :param M: maximum possible value in range
    Wraps ``x`` so m <= x <= M; but unlike ``bound()`` which
    truncates, ``wrap()`` wraps x around the coordinate system defined by m,M.\n
    For example, m = -180, M = 180 (degrees), x = 360 --> returns 0.
    """
    diff = M - m
    while x > M:
        x = x - diff
    while x < m:
        x = x + diff
    return x

def bound(x, m, M=None):
    """
    :param x: scalar
    Either have m as scalar, so bound(x,m,M) which returns m <= x <= M *OR*
    have m as length 2 vector, bound(x,m, <IGNORED>) returns m[0] <= x <= m[1].
    """
    if M is None:
        M = m[1]
        m = m[0]
    # bound x between min (m) and Max (M)
    return min(max(x, m), M)


def rk4(derivs, y0, t, *args, **kwargs):
	"""
	Integrate 1D or ND system of ODEs using 4-th order Runge-Kutta.
	This is a toy implementation which may be useful if you find
	yourself stranded on a system w/o scipy.Otherwise use
	:func:`scipy.integrate`.
	*y0*
		initial state vector
	*t*
		sample times
	*derivs*
		returns the derivative of the system and has the
		signature ``dy = derivs(yi, ti)``
	*args*
		additional arguments passed to the derivative function
	*kwargs*
		additional keyword arguments passed to the derivative function
	Example 1 ::
		## 2D system
		def derivs6(x,t):
			d1 =x[0] + 2*x[1]
			d2 =-3*x[0] + 4*x[1]
			return (d1, d2)
		dt = 0.0005
		t = arange(0.0, 2.0, dt)
		y0 = (1,2)
		yout = rk4(derivs6, y0, t)
	Example 2::
		## 1D system
		alpha = 2
		def derivs(x,t):
			return -alpha*x + exp(-t)
		y0 = 1
		yout = rk4(derivs, y0, t)
	If you have access to scipy, you should probably be using the
	scipy.integrate tools rather than this function.
	"""

	try:
		Ny = len(y0)
	except TypeError:
		yout = np.zeros((len(t),), np.float_)
	else:
		yout = np.zeros((len(t), Ny), np.float_)

	yout[0] = y0


	for i in np.arange(len(t) - 1):

		thist = t[i]
		dt = t[i + 1] - thist
		dt2 = dt / 2.0
		y0 = yout[i]

		k1 = np.asarray(derivs(y0, thist, *args, **kwargs))
		k2 = np.asarray(derivs(y0 + dt2 * k1, thist + dt2, *args, **kwargs))
		k3 = np.asarray(derivs(y0 + dt2 * k2, thist + dt2, *args, **kwargs))
		k4 = np.asarray(derivs(y0 + dt * k3, thist + dt, *args, **kwargs))
		yout[i + 1] = y0 + dt / 6.0 * (k1 + 2 * k2 + 2 * k3 + k4)
	return yout

