import lasagne
import lasagne.regularization as R
import lasagne.layers as L
import theano
import theano.tensor as T
import time
import numpy as np
import joblib

import sys
import os

num_epochs = 400

def var_reg(net, train_clip=False, thresh=3):
	return T.mean([layer.eval_reg(train_clip=False, thresh=thresh)
					for layer in lasagne.layers.get_all_layers(net) if 'reg' in layer.__dict__])

def res_reg(net, train_clip=False, thresh=3):
	return T.mean([layer.eval_reset(train_clip=False, thresh=thresh)
					for layer in lasagne.layers.get_all_layers(net) if 'reg' in layer.__dict__])

def network_noise(net, train_clip=False, thresh=3):
	return T.mean([layer.log_sigma2.mean()
					for layer in lasagne.layers.get_all_layers(net) if 'log_sigma2' in layer.__dict__])

def weight_layers(net):
	layers = L.get_all_layers(net)
	fctype = type(layers[-1])
	weights = {}
	for l in layers:
		if type(l) == fctype:
			weights[l] = 1e-4
	return weights

def iterate_minibatches(inputs, targets, batchsize, shuffle=False):
	assert len(inputs) == len(targets)
	indices = np.arange(len(targets))
	if shuffle:
		np.random.shuffle(indices)
	for start_idx in range(0, indices.size, batchsize):
		if shuffle:
			excerpt = indices[start_idx:start_idx + batchsize]
		else:
			excerpt = slice(start_idx, start_idx + batchsize)
		yield inputs[excerpt], targets[excerpt], excerpt

	

def load_data(folder):
	obs = np.load(folder + 'obs.npz')
	actions = np.load(folder + 'act.npz')
	#stds = np.load(folder + 'diff.npz')

	obs = np.concatenate([obs[entry] for entry in obs.files]).astype(np.float32)
	actions = np.concatenate([actions[entry] for entry in actions.files]).astype(np.float32)
	#stds = np.concatenate([stds[entry] for entry in stds.files]).astype(np.float32)

	assert obs.shape[0] ==  actions.shape[0]
	return obs, actions#, stds

def print_weights(network):
	for l in L.get_all_layers(network):
		if 'W' in l.__dict__:
			print(np.abs(l.W.get_value()).mean())

if __name__ == '__main__':
	print("prepare network")

	input_var = T.matrix('inputs')
	target = T.matrix('targets')
	src = "/rllab/data/local/experiment/experiment_2018_12_07_03_43_05_0001/itr_0.pkl"
	dest = "/rllab/data/local/experiment/networks/trained_carracing2.pkl"
	data = joblib.load(src)
	policy = data['policy']
	print("prepare data")
	obs, act = load_data('data/car_racing/demo_')
	divider = 20000
	X = obs[:divider]
	y = act[:divider]
	print("prepare training")
	dist = policy.distribution
	dist_info = policy.dist_info_sym(input_var, deterministic=True)
	mse = lasagne.objectives.squared_error(dist_info['mean'], target)
	likelihood = dist.log_likelihood_sym(target, dist_info)
	policy_outputs = [policy._l_mean, policy._l_log_std]
	pred = lasagne.layers.get_output(policy_outputs[0], input_var, deterministic=True)
	pred_error = lasagne.objectives.squared_error(pred, target)
	variational_regularization = var_reg(policy_outputs[0])
	l2_regularization = R.regularize_network_params(policy_outputs[0], R.l2,
		{'regularizable': True, 'variational' : False})
	print("prepare objective function")
	loss = -likelihood.mean() \
				+ 1e-5 * l2_regularization
	params = lasagne.layers.get_all_params(policy_outputs[0])
	updates = lasagne.updates.adam(loss, params)
	print("prepare training function")
	train_fn = theano.function([input_var, target], loss, updates=updates)
	reg_fn = theano.function([], [variational_regularization, l2_regularization])
	print("prepare evaluation function")
	eval_fn = theano.function([input_var, target], pred_error.mean(axis=0))
	print("begin training...")
	for epoch in range(num_epochs):
		if epoch % 50 == 0 and epoch != 0:
			print_weights(policy_outputs[0])
			joblib.dump(data, dest)
		train_err = 0
		eval_err = 0
		#train_err1 = 0
		train_batches = 0
		uncertainties = 0
		start_time = time.time()
		for batch in iterate_minibatches(X, y, 200, shuffle=True):
			inputs, targets, _ = batch
			train_err += train_fn(inputs, targets)
			eval_err += eval_fn(inputs, targets)
			train_batches += 1
	
		print("Epoch {} of {} took {:.3f}s".format(
			epoch+1, num_epochs, time.time() - start_time))
		vreg, l2reg =  reg_fn()
		print("\tvariational regularization loss:\t{}".format(vreg))
		print("\tl2 regularization loss:\t{}".format(l2reg))
		print("\ttraining loss:\t\t{:.6f}".format(train_err / train_batches))
		print("\tpred error:\t\t{}".format(eval_err / train_batches))
	print_weights(policy_outputs[0])
	joblib.dump(data, dest)
	sys.exit()
#
	#X = obs[:divider]
	#y = act[:divider]

	#det_pred = lasagne.layers.get_output(policy_outputs[0], input_var, deterministic=True)
	#det_error = lasagne.objectives.squared_error(det_pred[:,:-1], target)
	#eval_fn = theano.function([input_var, target], det_error.mean())
	#err = 0
	#batches = 0
	#for batch in iterate_minibatches(X, y, 200, shuffle=False):
		#inputs, targets, indices = batch
		#err += eval_fn(inputs, targets)
		#batches += 1
	#print("\teval error:\t{}".format(err/batches))

	#output_var = lasagne.layers.get_output(policy_outputs[0], input_var, train_clip=True)[:,:-1]
	#eval_fn = theano.function([input_var], output_var)
	#uncertainties = np.zeros_like(y)
	#for batch in iterate_minibatches(X, y, 200, shuffle=False):
		#inputs, _, indices = batch
		#print(indices)
		#predictions = np.array([eval_fn(inputs) for _ in range(100)])
		#uncertainties[indices] = predictions.std(axis=0)
	#print("\teval error:\t{}".format(uncertainties))
	#np.savez_compressed("stds5.npz", uncertainties)
	#sys.exit()

	std_pred = lasagne.layers.get_output(policy_outputs[1], input_var, deterministic=False, train_clip=True)
	std_pred_error = T.abs_(std_pred - target).mean()
	std_variational_regularization = var_reg(policy_outputs[1])
	loss = std_pred_error \
			+ 1 * std_variational_regularization
	std_params = lasagne.layers.get_all_params(policy_outputs[1])
	std_updates = lasagne.updates.adam(loss, std_params)
	print("compile std functions")
	train_fn = theano.function([input_var, target], loss, updates=std_updates)
	reg_fn = theano.function([], std_variational_regularization)
	eval_fn = theano.function([input_var, target], std_pred_error)
	#larray = dist.log_likelihood_array(target, dist_info)
	#eval_fn = theano.function([input_var, target], larray.mean(axis=0))
	num_epochs = 100
	#stds = stds/stds.mean(axis=0)*np.exp(-2.5)
	y = np.log(stds[:divider])
	for epoch in range(num_epochs):
		train_err = 0
		eval_err = 0
		train_batches = 0
		start_time = time.time()
		for batch in iterate_minibatches(X, y, 200, shuffle=True):
			inputs, targets, _ = batch
			err = train_fn(inputs, targets)
			#print("train_err = %f" %err)
			train_err += err
			#eval_err += eval_fn(inputs, targets)
			train_batches += 1
	
		print("Epoch {} of {} took {:.3f}s".format(
			epoch+1, num_epochs, time.time() - start_time))
		#print("\teval err:\t\t{}".format(eval_err / train_batches))
		print("\tvariational regularization loss:\t{}".format(reg_fn()))
		print("\ttraining loss_0:\t\t{:.6f}".format(train_err / train_batches))

	err= 0.
	batches = 0
	for batch in iterate_minibatches(X, y, 200, shuffle=False):
		inputs, targets, indices = batch
		err += eval_fn(inputs, targets)
		batches += 1
	print("\teval error:\t{}".format(err/batches))
	print("save policy")

	joblib.dump(data, "/rllab/data/local/experiment/networks/trained_vd_std_actual.pkl")
