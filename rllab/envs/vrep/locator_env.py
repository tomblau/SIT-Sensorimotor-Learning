from rllab import spaces
from rllab.envs.base import Env
import vrep
from math import pi
import time
import sys
import numpy as np
import subprocess

PRETRAIN_LENGTH = 200000
RESOLUTION = 128
N_CHANNELS = 4
N_JOINTS = 6
XBOUNDS = (-0.5, 0.3)
YBOUNDS = (-0.3, 0.2)
Z = 0.262

class LocatorEnv(Env):
	def __init__(self):
		port = np.random.randint(10000, 19999)
		subprocess.Popen(["./vrep.sh", "-h", "-gREMOTEAPISERVERSERVICE_%d_FALSE_TRUE" %port, "scenes/locator.ttt"])
		print(port)
		time.sleep(15)
		vrep.simxFinish(-1)
		self.clientID=vrep.simxStart('127.0.0.1',port,True,True,5000,5)
		if self.clientID == -1:
			sys.exit("failed to connect to sim")
		print("connected to sim")
		res = vrep.simxStartSimulation(self.clientID, vrep.simx_opmode_oneshot)
		time.sleep(1)
		res = vrep.simxSynchronous(self.clientID,True)
		if res > 1:
			sys.exit("failed to initiate synchronous mode")

		res, self.rgb_sensor=vrep.simxGetObjectHandle(self.clientID,"kinect_rgb",vrep.simx_opmode_blocking)
		vrep.simxGetVisionSensorImage(self.clientID, self.rgb_sensor, 0, vrep.simx_opmode_streaming)
		if res > 1:
			sys.exit("failed to acquire rgb sensor")
		res, self.depth_sensor=vrep.simxGetObjectHandle(self.clientID,"kinect_depth",vrep.simx_opmode_blocking)
		vrep.simxGetVisionSensorDepthBuffer(self.clientID, self.depth_sensor, vrep.simx_opmode_streaming)
		if res > 1:
			sys.exit("failed to acquire depth sensor")

		res, self.hand = vrep.simxGetObjectHandle(self.clientID,"JacoHand",vrep.simx_opmode_blocking)
		if res > 1:
			sys.exit("failed to acquire hand")
		self.pos =  vrep.simxGetObjectPosition(self.clientID, self.hand, -1, vrep.simx_opmode_blocking)[-1]

		res, self.target = vrep.simxGetObjectHandle(self.clientID,"Cup",vrep.simx_opmode_blocking)
		if res > 1:
			sys.exit("failed to acquire target")

		self.iter = 0

	def step(self, action):
		if self.iter <= PRETRAIN_LENGTH:
			demo = self.move_directly()
			self.iter += 1
		demo = self.move_directly()
		self.pos += action + demo
		x, y, z = self.pos[0], self.pos[1], self.pos[2]
		vrep.simxSetObjectPosition(self.clientID, self.hand, -1, (x, y, z), vrep.simx_opmode_blocking)
		vrep.simxSynchronousTrigger(self.clientID)
		observation = self.observe()
		if self.iter <= PRETRAIN_LENGTH:
			reward = -np.linalg.norm(demo - action)
		else:
			reward = self.get_reward(action)
		distance =  np.linalg.norm(self.pos - self.target_pos)
		#success state
		done = np.abs(distance) <= 5e-2
		if done:
			print("success")
			reward += 20
		#failure state
		if self.pos[-1] < 0.2:
			done = True
			reward = -100

		return observation, reward, done, {'action': demo}

	def move_directly(self):
		vector = self.target_pos - self.pos
		max_dim = np.abs(vector).max()
		dim_bound = self.action_space.high[0]
		if max_dim <= dim_bound:
			return vector
		return vector / max_dim * dim_bound

	def observe(self):
		_, _, obs_rgb = vrep.simxGetVisionSensorImage(self.clientID,self.rgb_sensor,0,vrep.simx_opmode_streaming)
		_, _, obs_depth = vrep.simxGetVisionSensorDepthBuffer(self.clientID,self.depth_sensor,vrep.simx_opmode_streaming)
		obs_rgb = np.asarray(obs_rgb).reshape(RESOLUTION, RESOLUTION, 3).astype(np.uint8)/255.
		obs_depth = np.asarray(obs_depth).reshape(RESOLUTION, RESOLUTION, 1)
		obs_depth /=  obs_depth.max()
		#concatenate rgb and depth readings
		observation = np.concatenate([obs_rgb, obs_depth], axis=2)
		observation = np.reshape(observation, (observation.size,))
		positions  = np.zeros((N_JOINTS,))
		observation = np.concatenate([observation, positions]).astype(np.float32)
		return observation

	def get_reward(self, action):
		dist_loss = np.linalg.norm(self.pos - self.target_pos)
		action_loss = np.linalg.norm(action)
		return -(dist_loss + action_loss)

	def reset(self):
		vrep.simxSynchronous(self.clientID, False)
		vrep.simxStopSimulation(self.clientID, vrep.simx_opmode_oneshot)
		time.sleep(1)
		vrep.simxStartSimulation(self.clientID, vrep.simx_opmode_oneshot)
		time.sleep(1)
		vrep.simxSynchronous(self.clientID, True)
		self.move_target()
		vrep.simxSynchronousTrigger(self.clientID)
		observation = self.observe()
		self.pos =  vrep.simxGetObjectPosition(self.clientID, self.hand, -1, vrep.simx_opmode_blocking)[-1]
		return observation

	def move_target(self):
		x = np.random.uniform(XBOUNDS[0], XBOUNDS[1])
		y = np.random.uniform(YBOUNDS[0], YBOUNDS[1])
		z = Z
		vrep.simxSetObjectPosition(self.clientID, self.target, -1, (x, y, z), vrep.simx_opmode_blocking)
		self.target_pos = np.array([x, y, z])

	def move_gripper(self):
		theta = np.random.uniform(0, 2*pi)
		omega = np.random.uniform(0, 2*pi)
		dist = self.action_space.high[0] * 25
		x = self.target_pos[0] + np.sin(theta) * np.cos(omega) * dist
		y = self.target_pos[1] + np.sin(theta) * np.sin(omega) * dist
		z = self.target_pos[2] + np.cos(theta) * dist
		vrep.simxSetObjectPosition(self.clientID, self.hand, -1, (x, y, z), vrep.simx_opmode_blocking)
		self.pos = np.array([x, y, z])

	@property
	def action_space(self):
		return spaces.Box(low=-0.05, high=0.05, shape=(3,))

	@property
	def observation_space(self):
		return spaces.Box(low=0, high=1, shape=(RESOLUTION * RESOLUTION * N_CHANNELS + N_JOINTS,))
