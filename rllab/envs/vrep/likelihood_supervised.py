import lasagne
import lasagne.regularization as R
import theano
import theano.tensor as T
from theano.compile.nanguardmode import NanGuardMode
import time
import numpy as np
import joblib

import sys
import os

num_epochs = 1000

def iterate_minibatches(inputs, targets, batchsize, shuffle=False):
	assert len(inputs) == len(targets)
	indices = np.arange(len(targets))
	if shuffle:
		np.random.shuffle(indices)
	for start_idx in range(0, indices.size, batchsize):
		if shuffle:
			excerpt = indices[start_idx:start_idx + batchsize]
		else:
			excerpt = slice(start_idx, start_idx + batchsize)
		yield inputs[excerpt], targets[excerpt], excerpt

	

def load_data(folder):
	obs = np.load(folder + 'obs.npz')
	actions = np.load(folder + 'act.npz')
	#joints = np.load(folder + 'backup/demo_joints.npz')

	obs = np.concatenate([obs[entry] for entry in obs.files]).astype(np.float32)
	actions = np.concatenate([actions[entry] for entry in actions.files]).astype(np.float32)

	assert obs.shape[0] ==  actions.shape[0]
	#length = obs.shape[0]
	#obs = obs.flatten().reshape((length, -1))
	#X = np.concatenate([obs,joints], axis=1)
	return obs, actions

if __name__ == '__main__':
	print("prepare network")

	input_var = T.matrix('inputs')
	target = T.matrix('targets')
	src = "/rllab/data/local/experiment/experiment_2018_01_11_00_00_07_0001/itr_0.pkl"
	data = joblib.load(src)
	policy = data['policy']
	print("prepare data")
	obs, act = load_data('data/demo_')
	act = act[:,:-1]
	X = obs[:]
	y = act[:]
	print("prepare training")
	#dist = policy.distribution
	#dist_info = policy.dist_info_sym(input_var)
	#dist_info["alpha"] = dist_info["alpha"][:,:-1]
	#dist_info["beta"] = dist_info["beta"][:,:-1]
	#larray = dist.log_likelihood_array(target, dist_info)
	#likelihood = dist.log_likelihood_sym(target, dist_info)
	policy_outputs = [policy._l_mean, policy._l_log_std]
	pred = lasagne.layers.get_output(policy_outputs[0], input_var)
	pred_error = lasagne.objectives.squared_error(pred[:,:-1], target)
	loss = pred_error.mean() \
				+ 1e-4*R.regularize_network_params(policy_outputs[0], R.l2) \
				+ 1e-4*R.regularize_network_params(policy_outputs[1], R.l2)
	#pred = lasagne.layers.get_output(policy_outputs[0], input_var)
	#noise = lasagne.layers.get_output(policy_outputs[1], input_var)
	#pred_error = lasagne.objectives.squared_error(pred, target)
	#log_error = T.log(pred_error)
	params = lasagne.layers.get_all_params(policy_outputs[0])
	updates = lasagne.updates.adam(loss, params)
	train_fn = theano.function([input_var, target], loss, updates=updates)
	eval_fn = theano.function([input_var], pred[:,:-1])
	#train_fn1 = theano.function([input_var, target], loss_1, updates=updates_1)
	print("begin training...")
	for epoch in range(num_epochs):
		train_err = 0
		eval_err = 0
		#train_err1 = 0
		train_batches = 0
		uncertainties = 0
		start_time = time.time()
		for batch in iterate_minibatches(X, y, 200, shuffle=True):
			inputs, targets, _ = batch
			train_err += train_fn(inputs, targets)
			#eval_err += eval_fn(inputs, targets)
			#train_err1 += train_fn1(inputs, targets)
			train_batches += 1
	
		print("Epoch {} of {} took {:.3f}s".format(
			epoch+1, num_epochs, time.time() - start_time))
		#print("\ttraining log error:\t{}".format(eval_res[0]))
		print("\ttraining loss_0:\t\t{:.6f}".format(train_err / train_batches))
		#print("\ttraining loss_1:\t\t{:.6f}".format(train_err1 / train_batches))
	joblib.dump(data, "/rllab/data/local/experiment/ddpg/trpo.pkl")
	sys.exit()
	
	uncertainties = np.zeros_like(y)
	train_batches = 0
	for batch in iterate_minibatches(X, y, 200, shuffle=False):
		inputs, _, indices = batch
		print(indices)
		predictions = np.array([eval_fn(inputs) for _ in range(10)])
		uncertainties[indices] = predictions.std(axis=0)
		train_batches += 1

	print("\teval error:\t{}".format(uncertainties / train_batches))
	np.savez_compressed("stds.npz", uncertainties)
	sys.exit()

	X = obs[:4000]
	y = act[:4000]
	std_params = lasagne.layers.get_all_params(policy_outputs[1], trainable=True)
	loss = -likelihood.mean() \
			+ 1e-4*R.regularize_network_params(policy_outputs[1], R.l2)
	std_updates = lasagne.updates.adam(loss, std_params)
	train_fn = theano.function([input_var, target], loss, updates=std_updates)
	#larray = dist.log_likelihood_array(target, dist_info)
	#eval_fn = theano.function([input_var, target], larray.mean(axis=0))
	num_epochs = 200
	for epoch in range(num_epochs):
		train_err = 0
		eval_err = 0
		train_batches = 0
		start_time = time.time()
		for batch in iterate_minibatches(X, y, 200, shuffle=True):
			inputs, targets = batch
			train_err += train_fn(inputs, targets)
			#eval_err += eval_fn(inputs, targets)
			train_batches += 1
	
		print("Epoch {} of {} took {:.3f}s".format(
			epoch+1, num_epochs, time.time() - start_time))
		#print("\teval err:\t\t{}".format(eval_err / train_batches))
		print("\ttraining loss_0:\t\t{:.6f}".format(train_err / train_batches))

	print("save policy")

	joblib.dump(data, "/rllab/data/local/experiment/ddpg/trpo.pkl")
