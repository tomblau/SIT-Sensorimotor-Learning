import lasagne
import lasagne.regularization as R
import theano
import theano.tensor as T
import time
import numpy as np
import joblib
from theano.compile.nanguardmode import NanGuardMode

import sys
import os


alpha = 1e-4
beta = 1e2
minibatch_size = 8000
num_epochs = 400

def iterate_minibatches(inputs, targets, batchsize, shuffle=False):
	assert len(inputs) == len(targets)
	indices = np.arange(len(targets))
	if shuffle:
		np.random.shuffle(indices)
	for start_idx in range(0, indices.size, batchsize):
		if shuffle:
			excerpt = indices[start_idx:start_idx + batchsize]
		else:
			excerpt = slice(start_idx, start_idx + batchsize)
		yield inputs[excerpt], targets[excerpt]


def load_data(folder):
	obs = np.load(folder + 'obs.npz')
	actions = np.load(folder + 'act.npz')
	#joints = np.load(folder + 'backup/demo_joints.npz')

	obs = np.concatenate([obs[entry] for entry in obs.files]).astype(np.float32)
	actions = np.concatenate([actions[entry] for entry in actions.files]).astype(np.float32)

	assert obs.shape[0] ==  actions.shape[0]
	#length = obs.shape[0]
	#obs = obs.flatten().reshape((length, -1))
	#X = np.concatenate([obs,joints], axis=1)
	return obs, actions

def softplus(x):
	return np.log(np.exp(x) + 1)

if __name__ == '__main__':
	print("prepare data")
	folder = str()

	print("prepare network")

	input_var = T.matrix('inputs')
	input_varp = T.matrix('inputsp')
	target = T.matrix('targets')
	targetp = T.matrix('targets')
	src = "/rllab/data/local/experiment/networks/untrained_lowdreaching_rffblrp3.pkl"
	dest = "/rllab/data/local/experiment/networks/trained_lowdreaching_rffblrp3.pkl"
	data = joblib.load(src)
	policy = data['policy']
	reg = policy._regression
	kernel = policy._kernel
	#init_ls = 1e0
	#policy._kernel.ls.set_value(np.log(np.exp(init_ls) - 1))
	X,y = load_data('data/lowd_half/demo_')
	data_limit = 8000
	divider = 4000
	X = X[:data_limit]
	y = y[:data_limit]
	inputs = X[:divider]
	targets = y[:divider]
	Xp = X[divider:]
	yp = y[divider:]
	
	dist = policy.distribution
	phi = policy.get_phis_sym(input_var)
	phip = policy.get_phis_sym(input_varp)
	dist_info = policy._regression.dist_info_sym(phi, phip, target, targetp)
	#dist_info = policy.dist_info_sym(input_var)
	likelihood = dist.log_likelihood_sym(targetp, dist_info)
	prediction = dist_info['mean']
	sqerror = T.sqr(prediction - targetp)
	pred_loss = sqerror.mean(axis=0)
	#gain = (dist_info['z']**2).sum(axis=2).mean()
	loss = -likelihood.mean()
	loss = loss + 1e-4*R.regularize_network_params(policy._l_out, R.l2,
		{"regularizable" : True, "variational" : False})
	params = lasagne.layers.get_all_params(policy._l_out, trainable=True, variational=False)
	#params.extend([reg.smin_alpha, reg.smin_beta])
	#params.extend(policy._kernel.get_params())
	#params.append(policy._regression.beta)
	updates = lasagne.updates.adam(loss, params)
	train_fn = theano.function([input_var, input_varp, target, targetp], loss, updates=updates)
		#mode=NanGuardMode(nan_is_error=True, inf_is_error=True, big_is_error=False))
	update_fn = policy._f_blr_update
	eval_fn = theano.function([input_var, input_varp, target, targetp],
		[pred_loss])#, gain, dist_info['sqerrors'], dist_info['l2s'], dist_info['dets'], dist_info['std'].mean(axis=0)])
	print("begin training...")
	indices = np.arange(X.shape[0])
	for epoch in range(num_epochs):
		np.random.shuffle(indices)
		inputs = X[indices[:divider]]
		targets = y[indices[:divider]]
		if epoch % 50 == 0 and epoch != 0:
			#print("compute features")
			#phi = np.zeros((X.shape[0], 128))
			#for i in range(int(X.shape[0]/200)):
				#lo, hi = i*200, (i+1)*200
				#phi[lo:hi] = lasagne.layers.get_output(policy._l_out, X[lo:hi], deterministic=True).eval()
			#print("learn noise")
			#policy._regression.learn_all_params(phi, y)
			#phi = 0
			print("save model")
			joblib.dump(data, dest)
		train_err = 0
		eval_err = 0
		gain_err = 0
		sqe_err = 0
		l2s_err = 0
		dets_err = 0
		std_err = 0
		train_batches = 0
		start_time = time.time()
		for batch in iterate_minibatches(X, y, minibatch_size, shuffle=True):
			inputsp, targetsp = batch
			#policy._regression.reset_params()
			#_ = update_fn(inputs, targets)
			train_err += train_fn(inputs, inputsp, targets, targetsp)
			if epoch % 10 == 0:
				eval_outputs = eval_fn(inputs, inputsp, targets, targetsp)
				eval_err += eval_outputs[0]  
				#gain_err += eval_outputs[1] 
				#sqe_err += eval_outputs[2]
				#l2s_err += eval_outputs[3]
				#dets_err += eval_outputs[4]
				#std_err += eval_outputs[5]
			train_batches += 1
	
		print("Epoch {} of {} took {:.3f}s".format(
			epoch+1, num_epochs, time.time() - start_time))
		print("\ttraining loss:\t\t{:.6f}".format(train_err / train_batches))
		#print("\tlengthscale:\t{}".format( softplus(policy._kernel.ls.get_value()) ))
		#print("\talpha:\t{}".format( softplus(reg.smin_alpha.get_value()) ))
		#print("\tbeta:\t{}".format( softplus(reg.smin_beta.get_value()) ))
		if epoch % 10 == 0:
			print("\tprediction loss: {}".format(eval_err / train_batches))
			#print("\tgain: {}".format(gain_err / train_batches))
			#print("\tsqe: {}".format(sqe_err / train_batches))
			#print("\tls2: {}".format(l2s_err / train_batches))
			#print("\tlogdets: {}".format(dets_err / train_batches))
			#print("\tstds: {}".format(std_err / train_batches))
	
	print("save policy")
	joblib.dump(data, dest)

	sys.exit()
