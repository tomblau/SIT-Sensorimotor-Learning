import lasagne
import lasagne.layers as L
import lasagne.regularization as R
import theano
import theano.tensor as T
import time
import numpy as np
import joblib

import sys
import os

num_epochs = 10

def var_reg(net, train_clip=False, thresh=3):
	return T.mean([layer.eval_reg(train_clip=False, thresh=thresh)
					for layer in lasagne.layers.get_all_layers(net) if 'reg' in layer.__dict__])

def iterate_minibatches(inputs, targets, batchsize, shuffle=False):
	assert len(inputs) == len(targets)
	indices = np.arange(len(targets))
	if shuffle:
		np.random.shuffle(indices)
	for start_idx in range(0, indices.size, batchsize):
		if shuffle:
			excerpt = indices[start_idx:start_idx + batchsize]
		else:
			excerpt = slice(start_idx, start_idx + batchsize)
		yield inputs[excerpt], targets[excerpt]

def load_data(folder):
	obs = np.load(folder + 'obs.npz')
	returns = np.load(folder + 'ret.npz')
	#joints = np.load(folder + 'backup/demo_joints.npz')

	obs = np.concatenate([obs[entry] for entry in obs.files]).astype(np.float32)
	returns = np.concatenate([returns[entry] for entry in returns.files]).astype(np.float32)

	assert obs.shape[0] ==  returns.shape[0]
	#length = obs.shape[0]
	#obs = obs.flatten().reshape((length, -1))
	#X = np.concatenate([obs,joints], axis=1)
	return obs, returns

if __name__ == '__main__':
	print("prepare data")
	folder = str()

	print("prepare network")

	data = joblib.load("/rllab/data/local/experiment/networks/trained_vd_baseline.pkl")
	regressor = data['baseline']._regressor
	X,y = load_data('data/ret/demo_')
	X = X[:2000]
	y = y[:2000]
	
	#mean = regressor._l_mean
	#log_std = regressor._l_log_std
	#mean_network = regressor._mean_network
	#x_mean_var = regressor._x_mean_var
	#x_std_var = regressor._x_std_var 
	#y_mean_var = regressor._y_mean_var
	#y_std_var = regressor._y_std_var
#
	#x_mean_var.set_value(
		#np.mean(X, axis=0, keepdims=True).astype(theano.config.floatX))
	#x_std_var.set_value(
		#(np.std(X, axis=0, keepdims=True) + 1e-8).astype(theano.config.floatX))
	#y_mean_var.set_value(
		#np.mean(y, axis=0, keepdims=True).astype(theano.config.floatX))
	#y_std_var.set_value(
		#(np.std(y, axis=0, keepdims=True) + 1e-8).astype(theano.config.floatX))
#
#
	#xs_var = T.matrix("xs")
	#ys_var = T.matrix("ys")
#
	#normalized_xs_var = xs_var
	#normalized_ys_var = ys_var
	#normalized_means_var = L.get_output(
		#mean, {mean_network.input_layer: normalized_xs_var},
		#deterministic=False, train_clip=True)
	#normalized_log_stds_var = L.get_output(
		#log_std, {mean_network.input_layer: normalized_xs_var},
		#deterministic=False, train_clip=True)
	#normalized_dist_info_vars = dict(
		#mean=normalized_means_var, log_std=normalized_log_stds_var)
#
	#loss = lasagne.objectives.squared_error(normalized_means_var, normalized_ys_var).mean()
	#reg_loss = loss + var_reg(mean) #+ var_reg(log_std)
	#params = L.get_all_params(mean)
	#updates = lasagne.updates.adam(reg_loss, params)
	#train_fn = theano.function([xs_var, ys_var], loss, updates=updates)
	#reg_fn = theano.function([], var_reg(mean) + var_reg(log_std))
	print("begin training...")
	for epoch in range(num_epochs):
		train_err = 0
		train_batches = 0
		start_time = time.time()
		for batch in iterate_minibatches(X, y, 200, shuffle=True):
			inputs, targets = batch
			regressor.fit(X,y)
			train_batches += 1
	
		print("Epoch {} of {} took {:.3f}s".format(
			epoch+1, num_epochs, time.time() - start_time))
		#print("\ttraining loss:\t\t{:.6f}".format(train_err / train_batches))
		#print("\tvariational regularization loss:\t{}".format(reg_fn()))

	joblib.dump(data, "/rllab/data/local/experiment/networks/trained_vd_baseline.pkl")
