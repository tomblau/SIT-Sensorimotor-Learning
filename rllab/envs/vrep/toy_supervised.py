import lasagne
import lasagne.regularization as R
import theano
import theano.tensor as T
import time
import numpy as np
import joblib
from theano.compile.nanguardmode import NanGuardMode

import sys
import os


alpha = 1e-4
beta = 1e2
minibatch_size = 200
num_epochs = 1000

def iterate_minibatches(inputs, targets, batchsize, shuffle=False):
	assert len(inputs) == len(targets)
	indices = np.arange(len(targets))
	if shuffle:
		np.random.shuffle(indices)
	for start_idx in range(0, indices.size, batchsize):
		if shuffle:
			excerpt = indices[start_idx:start_idx + batchsize]
		else:
			excerpt = slice(start_idx, start_idx + batchsize)
		yield inputs[excerpt], targets[excerpt]


def load_data(folder):
	obs = np.load(folder + 'obs.npz')
	actions = np.load(folder + 'tar.npz')
	#joints = np.load(folder + 'backup/demo_joints.npz')

	obs = np.concatenate([obs[entry] for entry in obs.files]).astype(np.float32)
	actions = np.concatenate([actions[entry] for entry in actions.files]).astype(np.float32)

	assert obs.shape[0] ==  actions.shape[0]
	return obs, actions

def softplus(x):
	return np.log(np.exp(x) + 1)

if __name__ == '__main__':
	print("prepare data")
	folder = str()

	print("prepare network")

	input_var = T.matrix('inputs')
	input_varp = T.matrix('inputsp')
	target = T.matrix('targets')
	targetp = T.matrix('targets')
	src = "/rllab/data/local/experiment/networks/toy_rffblrp.pkl"
	dest = "/rllab/data/local/experiment/networks/trained_toy_rffblrp3.pkl"
	data = joblib.load(src)
	policy = data['policy']
	reg = policy._regression
	kernel = policy._kernel
	init_ls = 1e-2
	softminus_ls = np.log(np.exp(init_ls) - 1)
	kernel.ls.set_value(np.ones_like(kernel.ls.get_value()) * softminus_ls)
	X,y = load_data('data/toy/demo_')
	data_limit = 8000
	divider = 2000
	X = X[:data_limit]
	y = y[:data_limit]
	inputs = X[:divider]
	targets = y[:divider]
	Xp = X[divider:]
	yp = y[divider:]
	
	dist = policy.distribution
	phi = policy.get_phis_sym(input_var)
	phip = policy.get_phis_sym(input_varp)
	dist_info = policy._regression.dist_info_sym(phi, phip, target, targetp)
	#dist_info = policy.dist_info_sym(input_var)
	likelihood = dist.log_likelihood_sym(targetp, dist_info)
	prediction = dist_info['mean']
	sqerror = T.sqr(prediction - targetp)
	pred_loss = sqerror.mean(axis=0)
	#gain = (dist_info['z']**2).sum(axis=2).mean()
	loss = -likelihood.mean()
	loss = loss + 1e-4*R.regularize_network_params(policy._l_out, R.l2,
		{"regularizable" : True, "variational" : False})
	params = lasagne.layers.get_all_params(policy._l_out, trainable=True, variational=False)
	#params.extend([reg.smin_alpha, reg.smin_beta])
	#params.extend(policy._kernel.get_params())
	#params.append(policy._regression.beta)
	updates = lasagne.updates.adam(loss, params)
	train_fn = theano.function([input_var, input_varp, target, targetp], loss, updates=updates)
	ls_loss = -dist_info['lml'].mean()
	ls_updates = lasagne.updates.adam(ls_loss, kernel.get_params())
	ls_fn = theano.function([input_var, input_varp, target, targetp], ls_loss, updates=ls_updates)
		#mode=NanGuardMode(nan_is_error=True, inf_is_error=True, big_is_error=False))
	update_fn = policy._f_blr_update
	eval_fn = theano.function([input_var, input_varp, target, targetp],
		[pred_loss])#, gain, dist_info['sqerrors'], dist_info['l2s'], dist_info['dets'], dist_info['std'].mean(axis=0)])
	print("begin training...")
	ls_err = 0
	train_err = 0
	for epoch in range(num_epochs):
		if (epoch+1) % 50 == 0:
			print("save model")
			joblib.dump(data, dest)
		indicator = (epoch // 25) % 2
		if indicator:
			ls_err = 0
		else:
			train_err = 0
		eval_err = 0
		train_batches = 0
		start_time = time.time()
		for batch in iterate_minibatches(X, y, minibatch_size, shuffle=True):
			inputsp, targetsp = batch
			if indicator:
				ls_err += ls_fn(inputs, inputsp, targets, targetsp)
			else:
				train_err += train_fn(inputs, inputsp, targets, targetsp)
			if epoch % 10 == 0:
				eval_outputs = eval_fn(inputs, inputsp, targets, targetsp)
				eval_err += eval_outputs[0]  
			train_batches += 1
	
		print("Epoch {} of {} took {:.3f}s".format(
			epoch+1, num_epochs, time.time() - start_time))
		print("\ttraining loss:\t\t{:.6f}".format(train_err / train_batches))
		print("\tls_loss:\t\t{:.6f}".format(ls_err / train_batches))
		print("\tlengthscale: {}".format(np.log(np.exp(kernel.ls.get_value())+1)))
		if epoch % 10 == 0:
			print("\tprediction loss: {}".format(eval_err / train_batches))
	
	print("save policy")
	joblib.dump(data, dest)

	sys.exit()
