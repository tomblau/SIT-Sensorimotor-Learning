import tensorflow as tf
import time
import numpy as np
import os

import sys

os.environ["CUDA_VISIBLE_DEVICES"]="3"
num_epochs = 100
reg = tf.contrib.layers.l2_regularizer(scale=1e-4)


def iterate_minibatches(inputs, targets, batchsize, shuffle=False):
	assert len(inputs) == len(targets)
	indices = np.arange(len(targets))
	if shuffle:
		np.random.shuffle(indices)
	for start_idx in range(0, indices.size, batchsize):
		if shuffle:
			excerpt = indices[start_idx:start_idx + batchsize]
		else:
			excerpt = slice(start_idx, start_idx + batchsize)
		yield inputs[excerpt], targets[excerpt], excerpt

	

def load_data(folder):
	obs = np.load(folder + 'obs.npz')
	actions = np.load(folder + 'act.npz')
	#stds = np.load(folder + 'diff.npz')

	obs = np.concatenate([obs[entry] for entry in obs.files]).astype(np.float32)
	actions = np.concatenate([actions[entry] for entry in actions.files]).astype(np.float32)
	#stds = np.concatenate([stds[entry] for entry in stds.files]).astype(np.float32)

	assert obs.shape[0] ==  actions.shape[0]
	#length = obs.shape[0]
	#obs = obs.flatten().reshape((length, -1))
	#X = np.concatenate([obs,joints], axis=1)
	return obs, actions#, stds

def print_weights(network):
	for l in L.get_all_layers(network):
		if 'W' in l.__dict__:
			print(np.abs(l.W.get_value()).mean())

def dense_layer(input_layer, output_dim, name_str):
	return tf.layers.dense(
			input_layer,
			output_dim,
			kernel_initializer=tf.glorot_uniform_initializer(),
			activation=tf.tanh,
			name=name_str,
			kernel_regularizer=reg
			)

def build_network():
	perception = tf.placeholder(tf.float32, [None, 128*128*4], name="per_input")
	proprioception = tf.placeholder(tf.float32, [None, 6], name="pro_input")

	per = tf.reshape(perception, [-1, 128, 128, 4], name="reshape")
	n_filters=(32,64,64)
	kernel_sizes=(6,6,4)
	strides=(2,)*3
	pads=("VALID",)*3
	pool_sizes=(2,2,2)
	for idx, n_filter, kernel_size, stride, pad, pool_size in zip(
		range(len(n_filters)),
		n_filters,
		kernel_sizes,
		strides,
		pads,
		pool_sizes
	):
		per = tf.layers.conv2d(
				per,
				n_filter,
				kernel_size,
				stride,
				pad,
				kernel_initializer=tf.glorot_uniform_initializer(),
				activation=tf.tanh,
				name="conv_%d" %idx,
				kernel_regularizer=reg
				)
		per = tf.layers.max_pooling2d(per,pool_size,pool_size, name="pool_%d" %idx)

	flat =  tf.layers.flatten(per, name="flat")
	per = flat
	pro = proprioception
	hidden_units=(256,256,64)
	for idx, n_units in enumerate(hidden_units):
		per = dense_layer(per, n_units, "per_dense_%d" %idx)
		pro = dense_layer(pro, n_units, "pro_dense_%d" %idx)

	combined = tf.concat([per, pro], axis=1, name="concat")
	outp = dense_layer(combined, 6, "output")

	target_var = tf.placeholder(tf.float32, [None, 6], name="target")
	reg_loss = tf.losses.get_regularization_loss(name="l2_loss")
	pred_loss = tf.reduce_mean((target_var - outp)**2, name="pred_loss")
	loss = tf.add(pred_loss, reg_loss, name="loss")
	train_op = tf.train.AdamOptimizer(learning_rate=1e-3).minimize(loss, name="train_op")

if __name__ == '__main__':
	print("prepare network")
	model_filepath=None
	#model_filepath="models/robo"
	config = tf.ConfigProto()
	config.gpu_options.per_process_gpu_memory_fraction = 0.2
	sess = tf.Session(config=config)

	print("prepare network")
	
	if model_filepath:
		print("loading network from: %s" %model_filepath)
		saver = tf.train.import_meta_graph("%s.meta" %model_filepath, clear_devices=True)
		model_directory = "/".join(model_filepath.split("/")[:-1])
		saver.restore(sess,tf.train.latest_checkpoint(model_directory))
	else:
		print("building network from scratch")
		with tf.device("/device:GPU:0"):
			build_network()

	graph = tf.get_default_graph()
	train_op = graph.get_operation_by_name("train_op")
	loss = graph.get_tensor_by_name("loss:0")
	pred_loss = graph.get_tensor_by_name("pred_loss:0")
	reg_loss = graph.get_tensor_by_name("l2_loss:0")
	outp = graph.get_tensor_by_name("output/Tanh:0")
	perception = graph.get_tensor_by_name("per_input:0")
	proprioception = graph.get_tensor_by_name("pro_input:0")
	target_var = graph.get_tensor_by_name("target:0")


	print("loading data")
	obs, act = load_data('data/half/demo_')
	divider = 8000
	X = obs[:divider]
	y = act[:divider]

	saver = tf.train.Saver()
	
	
	sess.run(tf.global_variables_initializer())
	print("begin training")
	for epoch in range(num_epochs):
		pred_error = 0
		reg_error = 0
		n_batches = 0
		t0 = time.time()
		for batch in iterate_minibatches(X, y, 200, shuffle=True):
			inputs, targets, _ = batch
			_, pred_err, reg_err, res = sess.run(
			[train_op, pred_loss, reg_loss, outp],
			feed_dict={
				perception : inputs[:,:-6],
				proprioception : inputs[:,-6:],
				target_var : targets
				}
			)
			pred_error += pred_err
			reg_error += reg_err
			n_batches += 1
		t1 = time.time()
		print("\nEpoch {} of {} took {:.3f}s".format(
			epoch+1, num_epochs, t1-t0))
		print("error = %f" %(pred_error/n_batches))
		print("regularisation = %f" %(reg_error/n_batches))

	saver.save(sess, "models/robo")
	sess.close()