from rllab import spaces
from rllab.envs.base import PropertyEnv
import rllab.envs.vrep.vrep as vrep
from math import pi
import time
import sys
import numpy as np
from cached_property import cached_property

N_JOINTS = 6
N_COORDS = 3
RESOLUTION = 256
N_CHANNELS = 4
XBOUNDS = (-0.565, -0.165)
YBOUNDS = (-0.2, 0.19)
Z = 0.25
POS_LB = np.array([-0.565, -0.2, 0.], dtype=np.float32)
POS_UB = np.array([-0.165, 0.19, 1.], dtype=np.float32)
JOINT_POSITION_CODE = 15
DEFAULT_JOINT_POS = [0, pi, pi, pi, pi, pi]
UNBOUND_JOINTS = [0, 3, 4, 5]
SUCC_REWARD = 25.
STEP_REWARD = -1.
FAIL_REWARD = -25.
INIT_ALPHA = 1


class JacoEnv(PropertyEnv):
	def __init__(self):
		vrep.simxFinish(-1)
		self.clientID=vrep.simxStart('127.0.0.1',19997,True,True,5000,5)
		if self.clientID == -1:
			sys.exit("failed to connect to sim")
		print("connected to sim")
		vrep.simxStartSimulation(self.clientID, vrep.simx_opmode_blocking)
		time.sleep(1)
		res = vrep.simxSynchronous(self.clientID,True)
		if res > 1:
			sys.exit("failed to initiate synchronous mode")

		res, self.rgb_sensor=vrep.simxGetObjectHandle(self.clientID,"kinect_rgb",vrep.simx_opmode_blocking)
		vrep.simxGetVisionSensorImage(self.clientID, self.rgb_sensor, 0, vrep.simx_opmode_streaming)
		if res > 1:
			sys.exit("failed to acquire rgb sensor")
		res, self.depth_sensor=vrep.simxGetObjectHandle(self.clientID,"kinect_depth",vrep.simx_opmode_blocking)
		vrep.simxGetVisionSensorDepthBuffer(self.clientID, self.depth_sensor, vrep.simx_opmode_streaming)
		if res > 1:
			sys.exit("failed to acquire depth sensor")

		res, self.tip = vrep.simxGetObjectHandle(self.clientID, "Jaco_tip", vrep.simx_opmode_blocking)
		if res > 1:
			sys.exit("failed to acquire Jaco tip")
		res, self.target = vrep.simxGetObjectHandle(self.clientID,"Cylinder",vrep.simx_opmode_blocking)
		if res > 1:
			sys.exit("failed to acquire target")
		self.target_pos = np.array(vrep.simxGetObjectPosition(self.clientID, self.target, -1, vrep.simx_opmode_blocking)[-1])

		self.joints = [0 for _ in range(7)]
		self.joint_pos = np.zeros((N_JOINTS,))
		for i in range(1,N_JOINTS + 1):
			res, self.joints[i - 1] = vrep.simxGetObjectHandle(self.clientID, "Jaco_joint" + str(i), vrep.simx_opmode_blocking)
			if res != 0:
				sys.exit("failed to acquire joint %d" %i)
			res, self.joint_pos[i - 1] = vrep.simxGetJointPosition(self.clientID, self.joints[i - 1], vrep.simx_opmode_blocking)
			if res != 0:
				sys.exit("failed to acquire position for joint %d, error code: %d" %(i, res))

		res, self.joint_col = vrep.simxGetCollectionHandle(self.clientID, 'Joints', vrep.simx_opmode_blocking)
		if res != 0:
			sys.exit("failed to acquire joint collection")
		vrep.simxGetObjectGroupData(self.clientID, self.joint_col, JOINT_POSITION_CODE, vrep.simx_opmode_streaming)
		
		self.sim_time = 0
		self.last_o = None

	def step(self, action):
		action = np.maximum(action, self.action_space.low)
		action = np.minimum(action, self.action_space.high)
		for i in range(N_JOINTS):
			#compute new joint pos after action
			self.joint_pos[i] += action[i]
			if self.joint_pos[i] < self.joint_space.low[i]:
				self.joint_pos[i] = self.joint_space.low[i]
			elif self.joint_pos[i] > self.joint_space.high[i]:
				self.joint_pos[i] = self.joint_space.high[i]
			vrep.simxSetJointTargetPosition(self.clientID, self.joints[i], self.joint_pos[i], vrep.simx_opmode_oneshot)
		#simulate 0.5 seconds of movement. Should be enough to move pi/5 radians
		old_time = self.sim_time
		vrep.simxSynchronousTrigger(self.clientID)
		self.sim_time = self.get_time()
		#make sure the simulation actually updated
		while(old_time == self.sim_time):
			print("sleeping")
			vrep.simxSynchronousTrigger(self.clientID)
			time.sleep(0.01)
			self.sim_time = self.get_time()

		stuck = self.check_positions()
		observation = self.observe()
		if stuck > 0:
			reward = FAIL_REWARD
			done = True
			print("failure!!!")
		else:
			self.distances = self.get_distances()
			vertical_dist = np.linalg.norm(self.distances[-1])
			horizontal_dist = np.linalg.norm(self.distances[:-1])
			total_dist = np.linalg.norm(self.distances)
			done = horizontal_dist < 0.08 and vertical_dist < 0.03
			reward = SUCC_REWARD*done + STEP_REWARD
			if done:
				print("success!!!")

		return observation, reward, done, {}

	def get_time(self):
		while(True):
			res, _, sim_time, _, _ = vrep.simxCallScriptFunction(self.clientID, 'Jaco', vrep.sim_scripttype_childscript, 'getTime', [], [], [], bytearray(), vrep.simx_opmode_blocking)
			if res == 0:
				return sim_time[0]
			print(res)
			time.sleep(0.1)

	def check_positions(self, joint_tol=5e-2, target_tol=1e-1, angle_tol=pi/4):
		"""
		Check that the joints have all arrived at the target position and the grasping target
		hasn't been knocked over. Update the model if no fault is found.
		-----------------
		Input:
		joint_tol- the maximum distance a joint can be from its target position
		target_tol- the maximum distance the target can be moved
		-----------------
		Output:
		Returns True if any joint is too far away from its target position, or if the grasping
		target has moved too far.
		Returns False otherwise
		"""
		retval = 0
		true_pos = vrep.simxGetObjectGroupData(self.clientID, self.joint_col, JOINT_POSITION_CODE, vrep.simx_opmode_streaming)[-2][0:-1:2]
		for i in range(N_JOINTS):
			if np.abs(self.joint_pos[i] - true_pos[i]) >= joint_tol:
				retval = 1
				print("joints %d expected %f but was %f" %(i, self.joint_pos[i], true_pos[i]))
			self.joint_pos[i] = true_pos[i]
		angles = vrep.simxGetObjectOrientation(self.clientID, self.target, -1, vrep.simx_opmode_blocking)[-1]
		for angle in angles[:-1]:
			if np.abs(angle) >= angle_tol:
				print("cylinder tipped over %f" %(angle*180/pi))
				retval = 1
		return retval

	def observe(self):
		_, _, obs_rgb = vrep.simxGetVisionSensorImage(self.clientID,self.rgb_sensor,0,vrep.simx_opmode_streaming)
		_, _, obs_depth = vrep.simxGetVisionSensorDepthBuffer(self.clientID,self.depth_sensor,vrep.simx_opmode_streaming)
		obs_rgb = np.asarray(obs_rgb).reshape(RESOLUTION, RESOLUTION, 3).astype(np.uint8)/255.
		obs_depth = np.asarray(obs_depth).reshape(RESOLUTION, RESOLUTION, 1)
		obs_depth /=  obs_depth.max()
		#concatenate rgb and depth readings
		observation = np.concatenate([obs_rgb, obs_depth], axis=2)
		observation = np.reshape(observation, (observation.size,))
		#concatenate joints positions
		positions  = np.divide( np.mod(self.joint_pos, 2*pi), 2*pi )
		observation = np.concatenate([observation, positions]).astype(np.float32)
		self.last_o = observation
		return observation

	def get_distances(self):
		cup_pos = vrep.simxGetObjectPosition(self.clientID, self.target, self.tip, vrep.simx_opmode_blocking)[-1]
		return np.asarray(cup_pos)

	def get_reward(self, action, expert_action, distance, done):
		if (expert_action == -1).all():
			loss = -np.linalg.norm(action) - distance
		else:
			loss = - np.linalg.norm(expert_action - action)
		return loss + done * 20

	def reset(self):
		self.sim_time = 0
		vrep.simxStopSimulation(self.clientID, vrep.simx_opmode_blocking)
		self.wait_for_response(8)
		vrep.simxStartSimulation(self.clientID, vrep.simx_opmode_blocking)
		vrep.simxSynchronous(self.clientID, True)
		self.wait_for_response(0)
		self.move_target()
		self.joint_pos = np.array(DEFAULT_JOINT_POS)
		#self.move_arm()
		observation = self.observe()
		return observation

	def wait_for_response(self, expected):
		while(True):
			res, _, _, _, _ = vrep.simxCallScriptFunction(self.clientID, 'Jaco', vrep.sim_scripttype_childscript, 'getTime', [], [], [], bytearray(), vrep.simx_opmode_blocking)
			if res == expected:
				return

	def move_target(self):
		x = np.random.uniform(XBOUNDS[0], XBOUNDS[1])
		y = np.random.uniform(YBOUNDS[0], YBOUNDS[1])
		z = Z
		vrep.simxSetObjectPosition(self.clientID, self.target, -1, (x, y, z), vrep.simx_opmode_blocking)
		self.target_pos = np.array([x, y, z])

	def move_arm(self):
		noise = np.random.uniform(high=pi/8, size=(6,))
		self.joint_pos =  self.joint_pos + noise
		for i in range(N_JOINTS):
			vrep.simxSetJointPosition(self.clientID, self.joints[i], self.joint_pos[i], vrep.simx_opmode_blocking)

	def flatten(self, array):
		return np.reshape(array, (array.size,))

	@cached_property
	def action_space(self):
		bound = np.ones(N_JOINTS) * pi / 5
		return spaces.Box(-bound, bound)

	@cached_property
	def observation_space(self):
	    return spaces.Box(low=0, high=1, shape=(RESOLUTION * RESOLUTION * N_CHANNELS + N_JOINTS,))
	
	@cached_property
	def joint_space(self):
	    lbound = np.array([-10000, 42, 20, -10000, -10000, -10000])*pi/180
	    ubound = np.array([10000, 318, 340, 10000, 10000, 10000])*pi/180
	    return spaces.Box(lbound, ubound)


class LowDJacoEnv(JacoEnv):
	def __init__(self):
		vrep.simxFinish(-1)
		self.clientID=vrep.simxStart('127.0.0.1',19997,True,True,5000,5)
		if self.clientID == -1:
			sys.exit("failed to connect to sim")
		print("connected to sim")
		res = vrep.simxSynchronous(self.clientID,True)
		vrep.simxStartSimulation(self.clientID, vrep.simx_opmode_blocking)
		time.sleep(1)
		if res > 1:
			sys.exit("failed to initiate synchronous mode")
		res, self.tip = vrep.simxGetObjectHandle(self.clientID, "Jaco_tip", vrep.simx_opmode_blocking)
		if res > 1:
			sys.exit("failed to acquire Jaco tip")
		res, self.target = vrep.simxGetObjectHandle(self.clientID,"Cylinder",vrep.simx_opmode_blocking)
		if res > 1:
			sys.exit("failed to acquire target")
		self.target_pos = np.array(vrep.simxGetObjectPosition(self.clientID, self.target, -1, vrep.simx_opmode_blocking)[-1])

		self.joints = [0 for _ in range(7)]
		self.joint_pos = np.zeros((N_JOINTS,))
		for i in range(1,N_JOINTS + 1):
			res, self.joints[i - 1] = vrep.simxGetObjectHandle(self.clientID, "Jaco_joint" + str(i), vrep.simx_opmode_blocking)
			if res != 0:
				sys.exit("failed to acquire joint %d" %i)
			res, self.joint_pos[i - 1] = vrep.simxGetJointPosition(self.clientID, self.joints[i - 1], vrep.simx_opmode_blocking)
			if res != 0:
				sys.exit("failed to acquire position for joint %d, error code: %d" %(i, res))

		res, self.joint_col = vrep.simxGetCollectionHandle(self.clientID, 'Joints', vrep.simx_opmode_blocking)
		if res != 0:
			sys.exit("failed to acquire joint collection")
		vrep.simxGetObjectGroupData(self.clientID, self.joint_col, JOINT_POSITION_CODE, vrep.simx_opmode_streaming)
		
		self.sim_time = 0
		self.last_o = None

	def observe(self):
		j_positions  = np.divide( np.mod(self.joint_pos, 2*pi), 2*pi )
		t_position = (self.target_pos - POS_LB) / (POS_UB - POS_LB)
		observation = np.concatenate([j_positions, t_position]).astype(np.float32)
		return observation

	@cached_property
	def observation_space(self):
	    return spaces.Box(low=0., high=1., shape=(N_JOINTS + N_COORDS,))
	




class EntJacoEnv(JacoEnv):
	"""
	Entropy regularised Jaco env
	"""
	def __init__(self, alpha=INIT_ALPHA):
		super(EntJacoEnv, self).__init__()
		self.alpha = alpha

	def step(self, action, agent_info):
		#agent info must contain entropy information
		assert("entropy" in agent_info)
		observation, reward, done, env_info = super(EntJacoEnv, self).step(action)
		entropy = self.alpha * agent_info['entropy']
		reward += entropy
		env_info["entropy"] = entropy
		return observation, reward, done, env_info

	def evolve_alpha(self):
		self.alpha = np.maximum(1e-8, 0.99*self.alpha)

class DisplayEnv(JacoEnv):
	"""
	environment for creating videos of a policy in action
	"""
	def step(self, action):
		vrep.simxSetIntegerSignal(self.clientID, "hand", 0, vrep.simx_opmode_blocking)
		action = np.maximum(action, self.action_space.low)
		action = np.minimum(action, self.action_space.high)
		for i in range(N_JOINTS):
			#compute new joint pos after action
			self.joint_pos[i] += action[i]
			if self.joint_pos[i] < self.joint_space.low[i]:
				self.joint_pos[i] = self.joint_space.low[i]
			elif self.joint_pos[i] > self.joint_space.high[i]:
				self.joint_pos[i] = self.joint_space.high[i]
			vrep.simxSetJointTargetPosition(self.clientID, self.joints[i], self.joint_pos[i], vrep.simx_opmode_oneshot)
		#simulate movement until velocity is low for all joints for at least 50ms
		old_time = self.get_time()
		vrep.simxSynchronous(self.clientID, False)
		fast = True
		while(fast or self.get_time() - old_time < 0.05):
			vel = vrep.simxGetObjectVelocity(self.clientID, self.tip, vrep.simx_opmode_blocking)[1]
			fast = (np.abs(vel) > 0.1).any()
			if fast:
				old_time = self.get_time()
			time.sleep(0.01)
		
		stuck = self.check_positions()
		observation = self.observe()
		if stuck > 0:
			reward = FAIL_REWARD
			done = True
			print("failure!!!")
		else:
			self.distances = self.get_distances()
			vertical_dist = np.linalg.norm(self.distances[-1])
			horizontal_dist = np.linalg.norm(self.distances[:-1])
			total_dist = np.linalg.norm(self.distances)
			done = horizontal_dist < 0.04 and vertical_dist < 0.03
			reward = SUCC_REWARD*done + STEP_REWARD
			if done:
				print("success!!!")
				vrep.simxSetIntegerSignal(self.clientID, "hand", 1, vrep.simx_opmode_blocking)
				time.sleep(2)

		return observation, reward, done, {}
