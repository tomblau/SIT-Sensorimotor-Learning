from rllab import spaces
from rllab.envs.base import Env
import vrep
from math import pi
import time
import sys
import numpy as np

PRETRAIN_LENGTH = 0
N_JOINTS = 6
RESOLUTION = 256
N_CHANNELS = 4
XBOUNDS = (-0.565, -0.165)
YBOUNDS = (-0.2, 0.19)
Z = 0.262
JOINT_POSITION_CODE = 15

class JacoEnv(Env):
	def __init__(self):
		vrep.simxFinish(-1)
		self.clientID=vrep.simxStart('127.0.0.1',19997,True,True,5000,5)
		if self.clientID == -1:
			sys.exit("failed to connect to sim")
		print "connected to sim"
		vrep.simxStartSimulation(self.clientID, vrep.simx_opmode_blocking)
		time.sleep(1)
		res = vrep.simxSynchronous(self.clientID,True)
		if res > 1:
			sys.exit("failed to initiate synchronous mode")

		res, self.rgb_sensor=vrep.simxGetObjectHandle(self.clientID,"kinect_rgb",vrep.simx_opmode_blocking)
		vrep.simxGetVisionSensorImage(self.clientID, self.rgb_sensor, 0, vrep.simx_opmode_streaming)
		if res > 1:
			sys.exit("failed to acquire rgb sensor")
		res, self.depth_sensor=vrep.simxGetObjectHandle(self.clientID,"kinect_depth",vrep.simx_opmode_blocking)
		vrep.simxGetVisionSensorDepthBuffer(self.clientID, self.depth_sensor, vrep.simx_opmode_streaming)
		if res > 1:
			sys.exit("failed to acquire depth sensor")

		res, self.tip = vrep.simxGetObjectHandle(self.clientID, "Jaco_tip", vrep.simx_opmode_blocking)
		if res > 1:
			sys.exit("failed to acquire Jaco tip")
		res, self.target = vrep.simxGetObjectHandle(self.clientID,"Cup",vrep.simx_opmode_blocking)
		if res > 1:
			sys.exit("failed to acquire target")
		self.target_pos = np.array(vrep.simxGetObjectPosition(self.clientID, self.target, -1, vrep.simx_opmode_blocking)[-1])

		self.joints = [0 for _ in range(7)]
		self.joint_pos = np.zeros((N_JOINTS,))
		for i in range(1,N_JOINTS + 1):
			res, self.joints[i - 1] = vrep.simxGetObjectHandle(self.clientID, "Jaco_joint" + str(i), vrep.simx_opmode_blocking)
			if res != 0:
				sys.exit("failed to acquire joint %d" %i)
			res, self.joint_pos[i - 1] = vrep.simxGetJointPosition(self.clientID, self.joints[i - 1], vrep.simx_opmode_blocking)
			if res != 0:
				sys.exit("failed to acquire position for joint %d, error code: %d" %(i, res))

		res, self.joint_col = vrep.simxGetCollectionHandle(self.clientID, 'Joints', vrep.simx_opmode_blocking)
		if res != 0:
			sys.exit("failed to acquire joint collection")
		vrep.simxGetObjectGroupData(self.clientID, self.joint_col, JOINT_POSITION_CODE, vrep.simx_opmode_streaming)
		
		self.sim_time = 0

		self.iter = 0
		self.iter_mod = 0
		self.demo_actions = np.load("/rllab/rllab/envs/vrep/demo_actions.npz")['arr_0']
		self.demo_joints = np.load("/rllab/rllab/envs/vrep/demo_joints.npz")['arr_0']
		self.demo_obs = np.load("/rllab/rllab/envs/vrep/demo_obs.npz")['arr_0']
		self.demo_dists = np.load("/rllab/rllab/envs/vrep/demo_dists.npz")['arr_0']

	def step(self, action):
		if self.iter < PRETRAIN_LENGTH:
			if self.iter_mod == 0:
				index = self.iter / 1000
				self.demo_actions = np.load("/rllab/rllab/envs/vrep/demo_actions.npz")['arr_%d' %index].astype(np.float32)
				self.demo_joints = np.load("/rllab/rllab/envs/vrep/demo_joints.npz")['arr_%d' %index].astype(np.float32)
				self.demo_obs = np.load("/rllab/rllab/envs/vrep/demo_obs.npz")['arr_%d' %index].astype(np.float32)
				self.demo_dists = np.load("/rllab/rllab/envs/vrep/demo_dists.npz")['arr_%d' %index].astype(np.float32)
			action = self.demo_actions[self.iter_mod]
			observation = np.concatenate([self.demo_obs[self.iter_mod], self.demo_joints[self.iter_mod]])
			reward = -(self.demo_dists[self.iter_mod] + np.linalg.norm(action))
			done = self.iter % 25 == 24
			self.iter += 1
			self.iter_mod = self.iter % 1000
			print self.iter
			return observation, reward, done, {}

		for i in range(N_JOINTS):
			#compute new joint pos after action
			self.joint_pos[i] += action[i]
			if self.joint_pos[i] < self.joint_space.low[i]:
				self.joint_pos[i] = self.joint_space.low[i]
			elif self.joint_pos[i] > self.joint_space.high[i]:
				self.joint_pos[i] = self.joint_space.high[i]
			vrep.simxSetJointTargetPosition(self.clientID, self.joints[i], self.joint_pos[i], vrep.simx_opmode_oneshot)
		#simulate 0.5 seconds of movement. Should be enough to move pi/2 radians
		old_time = self.sim_time
		vrep.simxSynchronousTrigger(self.clientID)
		self.sim_time = self.get_time()
		#make sure the simulation actually updated
		while(old_time == self.sim_time):
			print "sleeping"
			time.sleep(0.01)
			self.sim_time = self.get_time()

		stuck = self.check_positions()
		observation = self.observe()
		if stuck > 0:
			reward = -100
			done = True
			if stuck == 2:
				done = True
		else:
			distance = self.get_distance()
			done = distance < 0.05
			reward = self.get_reward(action, distance) + done * 20
		return observation, reward, done, {}

	def get_time(self):
		while(True):
			res, _, sim_time, _, _ = vrep.simxCallScriptFunction(self.clientID, 'Jaco', vrep.sim_scripttype_childscript, 'getTime', [], [], [], bytearray(), vrep.simx_opmode_blocking)
			if res == 0:
				return sim_time[0]
			print res
			time.sleep(0.1)

	def check_positions(self, joint_tol=1e-2, target_tol=1e-1):
		"""
		Check that the joints have all arrived at the target position and the grasping target
		hasn't been knocked over. Update the model if no fault is found.
		-----------------
		Input:
		joint_tol- the maximum distance a joint can be from its target position
		target_tol- the maximum distance the target can be moved
		-----------------
		Output:
		Returns True if any joint is too far away from its target position, or if the grasping
		target has moved too far.
		Returns False otherwise
		"""
		#true_pos = vrep.simxGetObjectPosition(self.clientID, self.target, -1, vrep.simx_opmode_blocking)[-1]
		#true_pos = np.array(true_pos)
		#if np.linalg.norm(true_pos - self.target_pos) >= target_tol:
			#return 2
		#self.target_pos = true_pos
		retval = 0
		true_pos = vrep.simxGetObjectGroupData(self.clientID, self.joint_col, JOINT_POSITION_CODE, vrep.simx_opmode_streaming)[-2][0:-1:2]
		for i in range(N_JOINTS):
			if np.abs(self.joint_pos[i] - true_pos[i]) >= joint_tol:
				retval = 1
			self.joint_pos[i] = true_pos[i]
		return retval

	def observe(self):
		_, _, obs_rgb = vrep.simxGetVisionSensorImage(self.clientID,self.rgb_sensor,0,vrep.simx_opmode_streaming)
		_, _, obs_depth = vrep.simxGetVisionSensorDepthBuffer(self.clientID,self.depth_sensor,vrep.simx_opmode_streaming)
		obs_rgb = np.asarray(obs_rgb).reshape(RESOLUTION, RESOLUTION, 3).astype(np.uint8)/255.
		obs_depth = np.asarray(obs_depth).reshape(RESOLUTION, RESOLUTION, 1)
		obs_depth /=  obs_depth.max()
		#concatenate rgb and depth readings
		observation = np.concatenate([obs_rgb, obs_depth], axis=2)
		observation = np.reshape(observation, (observation.size,))
		#concatenate joints positions
		positions  = np.divide( np.mod(self.joint_pos, 2*pi), 2*pi )
		observation = np.concatenate([observation, positions]).astype(np.float32)
		return observation

	def get_distance(self):
		tip_pos = vrep.simxGetObjectPosition(self.clientID, self.tip, -1, vrep.simx_opmode_blocking)[-1]
		return np.linalg.norm(np.asarray(tip_pos) - self.target_pos)

	def get_reward(self, action, distance):
		action_loss = np.linalg.norm(action)
		return -distance - action_loss

	def reset(self):
		vrep.simxSynchronous(self.clientID, False)
		vrep.simxStopSimulation(self.clientID, vrep.simx_opmode_blocking)
		time.sleep(1)
		vrep.simxStartSimulation(self.clientID, vrep.simx_opmode_blocking)
		time.sleep(1)
		vrep.simxSynchronous(self.clientID, True)
		self.move_target()
		self.joint_pos = [0, pi, pi, pi, pi, pi]
		if self.iter < PRETRAIN_LENGTH:
			observation = np.concatenate([self.demo_obs[self.iter_mod], self.demo_joints[self.iter_mod]])
		else:
			observation = self.observe()
		return observation

	def move_target(self):
		x = np.random.uniform(XBOUNDS[0], XBOUNDS[1])
		y = np.random.uniform(YBOUNDS[0], YBOUNDS[1])
		z = Z
		vrep.simxSetObjectPosition(self.clientID, self.target, -1, (x, y, z), vrep.simx_opmode_blocking)
		self.target_pos = np.array([x, y, z])

	@property
	def action_space(self):
		bound = np.ones(N_JOINTS) * pi / 10
		return spaces.Box(-bound, bound)

	@property
	def observation_space(self):
	    return spaces.Box(low=0, high=1, shape=(RESOLUTION * RESOLUTION * N_CHANNELS + N_JOINTS,))
	
	@property
	def joint_space(self):
	    lbound = np.array([-10000, 42, 17, -10000, -10000, -10000])*pi/180
	    ubound = np.array([10000, 318, 343, 10000, 10000, 10000])*pi/180
	    return spaces.Box(lbound, ubound)
	
