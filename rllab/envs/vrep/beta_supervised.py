import lasagne
import lasagne.regularization as R
import theano
import theano.tensor as T
import time
import numpy as np
import joblib

import sys
import os

num_epochs = 200

def iterate_minibatches(inputs, targets_a, targets_b, batchsize, shuffle=False):
	assert len(inputs) == len(targets_a) == len(targets_b)
	indices = np.arange(len(inputs))
	if shuffle:
		np.random.shuffle(indices)
	for start_idx in range(0, indices.size, batchsize):
		if shuffle:
			excerpt = indices[start_idx:start_idx + batchsize]
		else:
			excerpt = slice(start_idx, start_idx + batchsize)
		yield inputs[excerpt], targets_a[excerpt], targets_b[excerpt]

def load_data(folder):
	obs = np.load(folder + 'obs.npz')
	params = np.load(folder + 'params.npz')
	#joints = np.load(folder + 'backup/demo_joints.npz')

	obs = np.concatenate([obs[entry] for entry in obs.files]).astype(np.float32)
	alphas = params['alphas'].astype(np.float32)
	betas = params['betas'].astype(np.float32)

	assert obs.shape[0] ==  alphas.shape[0] == betas.shape[0]
	#length = obs.shape[0]
	#obs = obs.flatten().reshape((length, -1))
	#X = np.concatenate([obs,joints], axis=1)
	return obs, alphas, betas

if __name__ == '__main__':
	print("prepare data")
	folder = str()

	print("prepare network")

	input_var = T.matrix('inputs')
	target_a = T.matrix('targets_a')
	target_b = T.matrix('targets_b')
	src = "/rllab/data/local/experiment/experiment_2017_10_03_00_30_18_0001/params.pkl"
	data = joblib.load(src)
	policy = data['policy']
	X ,alphas, betas = load_data('data/beta/demo_')
	policy_outputs = [policy._l_alpha, policy._l_beta]
	pred_a = lasagne.layers.get_output(policy_outputs[0], input_var)
	pred_b = lasagne.layers.get_output(policy_outputs[1], input_var)
	loss_a = lasagne.objectives.squared_error(pred_a, target_a).mean()
	loss_b = lasagne.objectives.squared_error(pred_b, target_b).mean()
	loss = loss_a \
				+ 1e-4*R.regularize_network_params(policy_outputs[0], R.l2) \
				#+ 1e-4*R.regularize_network_params(policy_outputs[1], R.l2)
	params = lasagne.layers.get_all_params(policy_outputs[0])
	updates = lasagne.updates.adam(loss, params)
	train_fn = theano.function([input_var, target_a], loss, updates=updates)
	print("begin training...")
	for epoch in range(num_epochs):
		train_err = 0
		train_batches = 0
		start_time = time.time()
		for batch in iterate_minibatches(X, alphas, betas, 200, shuffle=True):
			inputs, targets_a, targets_b = batch
			train_err += train_fn(inputs, targets_a)
			train_batches += 1
	
		print("Epoch {} of {} took {:.3f}s".format(
			epoch+1, num_epochs, time.time() - start_time))
		print("\ttraining loss:\t\t{:.6f}".format(train_err / train_batches))
	
	print("save policy")

	joblib.dump(data, "/rllab/data/local/experiment/networks/trained_beta3.pkl")
