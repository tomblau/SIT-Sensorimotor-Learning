from rllab import spaces
from rllab.envs.base import Env
import vrep
from math import pi
import time
import sys
import numpy as np
import joblib

PRETRAIN_LENGTH = 0
PRETRAIN_START = 0
PRETRAIN_END = PRETRAIN_START + PRETRAIN_LENGTH
N_JOINTS = 6
RESOLUTION = 128
N_CHANNELS = 4
XBOUNDS = (-0.565, -0.165)
YBOUNDS = (-0.2, 0.19)
Z = 0.262
JOINT_POSITION_CODE = 15
DEFAULT_JOINT_POS = [0, pi, pi, pi, pi, pi]
OMPL_PREPARE = 0
GET_GOAL = 2
TIMEOUT_CODE = 2
UNBOUND_JOINTS = [0, 3, 4, 5]
ERROR_ACTION = np.array([-1])
base_orient = (-0.5*pi, 0, 0)
vertical_orient = (pi, 0, 0)
ompl_prepare = 0
get_goal = 2
timeout_code = 2
DEFAULT_MIXIN = 0.0


def shift_config(config):
	"""shift the config so that the values of all unbounded joints
		are in the range [-pi,pi]"""
	res = config
	for idx in UNBOUND_JOINTS:
		res[idx] = np.mod(res[idx], 2*pi)
		if res[idx] > pi:
			res[idx] = res[idx] - 2*pi
		if np.abs(res[idx]) > pi + 0.1:
			sys.exit("failed to shift vector element at index %d" %idx)
	return res

def find_nearest_goal(start, goal_set):
	min_goal = np.ones((N_JOINTS,))
	min_vector = np.ones((N_JOINTS,))*pi
	for goal in goal_set:
		goal = shift_config(np.mod(goal, 2*pi))
		vector = shift_config(goal - start)
		if distance_metric(vector) < distance_metric(min_vector):
			min_goal = goal
			min_vector = vector
	return min_goal, min_vector

def distance_metric(vector):
	"""
		The distance between two configs is the sum of angle differences
		excluding the final joint because it can take arbitrary values
	"""
	return np.sum(np.abs(vector)[:-1])


class ControlledEnv(Env):
	def __init__(self):
		vrep.simxFinish(-1)
		self.clientID=vrep.simxStart('127.0.0.1',19997,True,True,5000,5)
		if self.clientID == -1:
			sys.exit("failed to connect to sim")
		print("connected to sim")
		vrep.simxStartSimulation(self.clientID, vrep.simx_opmode_blocking)
		time.sleep(1)
		res = vrep.simxSynchronous(self.clientID,True)
		if res > 1:
			sys.exit("failed to initiate synchronous mode")

		res, self.rgb_sensor=vrep.simxGetObjectHandle(self.clientID,"kinect_rgb",vrep.simx_opmode_blocking)
		vrep.simxGetVisionSensorImage(self.clientID, self.rgb_sensor, 0, vrep.simx_opmode_streaming)
		if res > 1:
			sys.exit("failed to acquire rgb sensor")
		res, self.depth_sensor=vrep.simxGetObjectHandle(self.clientID,"kinect_depth",vrep.simx_opmode_blocking)
		vrep.simxGetVisionSensorDepthBuffer(self.clientID, self.depth_sensor, vrep.simx_opmode_streaming)
		if res > 1:
			sys.exit("failed to acquire depth sensor")

		res, self.tip = vrep.simxGetObjectHandle(self.clientID, "Jaco_tip0", vrep.simx_opmode_blocking)
		if res > 1:
			sys.exit("failed to acquire Jaco tip")
		res, self.target = vrep.simxGetObjectHandle(self.clientID,"Cup",vrep.simx_opmode_blocking)
		if res > 1:
			sys.exit("failed to acquire target")
		self.target_pos = np.array(vrep.simxGetObjectPosition(self.clientID, self.target, -1, vrep.simx_opmode_blocking)[-1])
		res, self.dummy = vrep.simxGetObjectHandle(self.clientID,"jacoTarget1",vrep.simx_opmode_blocking)
		if res > 1:
			sys.exit("failed to acquire target dummy")

		self.joints = [0 for _ in range(7)]
		self.joint_pos = np.zeros((N_JOINTS,))
		for i in range(1,N_JOINTS + 1):
			res, self.joints[i - 1] = vrep.simxGetObjectHandle(self.clientID, "Jaco_joint" + str(i), vrep.simx_opmode_blocking)
			if res != 0:
				sys.exit("failed to acquire joint %d" %i)
			res, self.joint_pos[i - 1] = vrep.simxGetJointPosition(self.clientID, self.joints[i - 1], vrep.simx_opmode_blocking)
			if res != 0:
				sys.exit("failed to acquire position for joint %d, error code: %d" %(i, res))

		res, self.joint_col = vrep.simxGetCollectionHandle(self.clientID, 'Joints', vrep.simx_opmode_blocking)
		if res != 0:
			sys.exit("failed to acquire joint collection")
		vrep.simxGetObjectGroupData(self.clientID, self.joint_col, JOINT_POSITION_CODE, vrep.simx_opmode_streaming)
		
		self.sim_time = 0

		self.goals = []
		self.mixin_ratio = DEFAULT_MIXIN
		self.evaluation = False

	def step(self, action, agent_info=None):
		vector = None
		if self.evaluation == False:
			vector = self.get_vector()
			if np.random.uniform() < self.mixin_ratio:
				action = vector + np.random.normal(0, 0.1, size=vector.shape)
		for i in range(N_JOINTS):
			#compute new joint pos after action
			self.joint_pos[i] += action[i]
			if self.joint_pos[i] < self.joint_space.low[i]:
				self.joint_pos[i] = self.joint_space.low[i]
			elif self.joint_pos[i] > self.joint_space.high[i]:
				self.joint_pos[i] = self.joint_space.high[i]
			vrep.simxSetJointTargetPosition(self.clientID, self.joints[i], self.joint_pos[i], vrep.simx_opmode_oneshot)
		#simulate 0.5 seconds of movement. Should be enough to move pi/2 radians
		old_time = self.sim_time
		vrep.simxSynchronousTrigger(self.clientID)
		vrep.simxSynchronousTrigger(self.clientID)
		self.sim_time = self.get_time()
		#make sure the simulation actually updated
		while(old_time == self.sim_time):
			print("sleeping")
			vrep.simxSynchronousTrigger(self.clientID)
			time.sleep(0.01)
			self.sim_time = self.get_time()

		stuck = self.check_positions()
		observation = self.observe()
		if stuck > 0:
			reward = -100
			done = True
			if stuck == 2:
				done = True
			if done:
				print("failure!!!")
		else:
			distances = self.get_distances()
			vertical_dist = np.linalg.norm(distances[-1])
			horizontal_dist = np.linalg.norm(distances[:-1])
			total_dist = np.linalg.norm(distances)
			done = horizontal_dist < 0.08 and vertical_dist < 0.035
			#done = total_dist < 0.2
			reward = 20*done - total_dist - np.linalg.norm(action)
			if done:
				print("success!!!")
		return observation, reward, done, {'decision' : vector, 'cup_pos': self.target_pos}

	def get_time(self):
		while(True):
			res, _, sim_time, _, _ = vrep.simxCallScriptFunction(self.clientID, 'Jaco', vrep.sim_scripttype_childscript, 'getTime', [], [], [], bytearray(), vrep.simx_opmode_blocking)
			if res == 0:
				return sim_time[0]
			print(res)
			time.sleep(0.1)

	def check_positions(self, joint_tol=1e-2, target_tol=1e-1):
		"""
		Check that the joints have all arrived at the target position and the grasping target
		hasn't been knocked over. Update the model if no fault is found.
		-----------------
		Input:
		joint_tol- the maximum distance a joint can be from its target position
		target_tol- the maximum distance the target can be moved
		-----------------
		Output:
		Returns True if any joint is too far away from its target position, or if the grasping
		target has moved too far.
		Returns False otherwise
		"""
		#true_pos = vrep.simxGetObjectPosition(self.clientID, self.target, -1, vrep.simx_opmode_blocking)[-1]
		#true_pos = np.array(true_pos)
		#if np.linalg.norm(true_pos - self.target_pos) >= target_tol:
			#return 2
		#self.target_pos = true_pos
		retval = 0
		true_pos = vrep.simxGetObjectGroupData(self.clientID, self.joint_col, JOINT_POSITION_CODE, vrep.simx_opmode_streaming)[-2][0:-1:2]
		for i in range(N_JOINTS):
			if np.abs(self.joint_pos[i] - true_pos[i]) >= joint_tol:
				retval = 1
				print("joints %d expected %f but was %f" %(i, self.joint_pos[i], true_pos[i]))
			self.joint_pos[i] = true_pos[i]
		return retval

	def observe(self):
		_, _, obs_rgb = vrep.simxGetVisionSensorImage(self.clientID,self.rgb_sensor,0,vrep.simx_opmode_streaming)
		_, _, obs_depth = vrep.simxGetVisionSensorDepthBuffer(self.clientID,self.depth_sensor,vrep.simx_opmode_streaming)
		obs_rgb = np.asarray(obs_rgb).reshape(RESOLUTION, RESOLUTION, 3).astype(np.uint8)/255.
		obs_depth = np.asarray(obs_depth).reshape(RESOLUTION, RESOLUTION, 1)
		obs_depth /=  obs_depth.max()
		#concatenate rgb and depth readings
		observation = np.concatenate([obs_rgb, obs_depth], axis=2)
		observation = np.reshape(observation, (observation.size,))
		#concatenate joints positions
		positions  = np.divide( np.mod(self.joint_pos, 2*pi), 2*pi )
		observation = np.concatenate([observation, positions]).astype(np.float32)
		return observation

	def get_distances(self):
		cup_pos = vrep.simxGetObjectPosition(self.clientID, self.target, self.tip, vrep.simx_opmode_blocking)[-1]
		return np.asarray(cup_pos)

	def get_reward(self, action, expert_action, distance, done):
		if (expert_action == -1).all():
			loss = -np.linalg.norm(action) - distance
		else:
			loss = - np.linalg.norm(expert_action - action)
		return loss + done * 20

	def reset(self):
		vrep.simxSynchronous(self.clientID, False)
		vrep.simxStopSimulation(self.clientID, vrep.simx_opmode_blocking)
		time.sleep(1)
		vrep.simxStartSimulation(self.clientID, vrep.simx_opmode_blocking)
		time.sleep(1)
		vrep.simxSynchronous(self.clientID, True)
		self.move_target()
		if self.evaluation == False:
			self.goals = self.find_goals()
		#self.sim_time = self.get_time()
		#self.joint_pos = np.array(vrep.simxGetObjectGroupData(self.clientID, self.joint_col, JOINT_POSITION_CODE, vrep.simx_opmode_streaming)[-2][0:-1:2])
		self.joint_pos = np.array([0, pi, pi, pi, pi, pi])
		observation = self.observe()
		return observation

	def move_target(self):
		x = np.random.uniform(XBOUNDS[0], XBOUNDS[1])
		y = np.random.uniform(YBOUNDS[0], YBOUNDS[1])
		z = Z	
		vrep.simxSetObjectPosition(self.clientID, self.target, -1, (x, y, z),
			vrep.simx_opmode_blocking)
		self.target_pos = np.array([x, y, z])

	def find_goals(self):
		dummy_pos = self.target_pos + np.array([0, 0, 0.04])
		#move self.target a little higher
		goals = []
		vrep.simxSetObjectPosition(self.clientID, self.dummy, -1, tuple(dummy_pos), vrep.simx_opmode_blocking)
		vrep.simxSynchronousTrigger(self.clientID)
		for i in range(2):
			for j in range(5):
				self.reset_config()
				vrep.simxSetObjectOrientation(self.clientID, self.dummy, self.dummy, (0, -0.25 * j * pi, 0), vrep.simx_opmode_blocking)
				vrep.simxSetObjectOrientation(self.clientID, self.dummy, self.dummy, (-0.25 * i * pi, 0, 0), vrep.simx_opmode_blocking)
				self.add_goal_configs(goals)
		vrep.simxSetObjectOrientation(self.clientID, self.dummy, -1, vertical_orient, vrep.simx_opmode_blocking)
		self.add_goal_configs(goals)
		return np.concatenate(goals)

	def reset_config(self):
		vrep.simxSetObjectOrientation(self.clientID, self.dummy, -1, base_orient, vrep.simx_opmode_blocking)

	def add_goal_configs(self, goals_list):
		inInts = [ompl_prepare]
		inFloats = []
		inStrings = []
		inBuffer = bytearray()
		errCode, ints, floats, strings, buffer = vrep.simxCallScriptFunction(
			self.clientID, "Jaco",vrep.sim_scripttype_childscript, "approachTarget",
			inInts, inFloats, inStrings, inBuffer, vrep.simx_opmode_blocking)
	
		if errCode >= 8:
			print("errcode: %d" %errCode)
			return
	
		inInts = [get_goal]
		errCode = timeout_code
		#while the bit for timeout in the error code is true
		while errCode & timeout_code:
			errCode, ints, floats, strings, buffer = vrep.simxCallScriptFunction(
				self.clientID, "Jaco", vrep.sim_scripttype_childscript, "approachTarget",
				inInts, inFloats, inStrings, inBuffer, vrep.simx_opmode_blocking)
		if len(ints) == 0 or ints[0] == 0 or ints[0]%N_JOINTS != 0:
			print("no valid goal pose found for config")
			return
		goals = np.array(floats).reshape(-1, N_JOINTS)
		goals_list.append(goals)

	def flatten(self, array):
		return np.reshape(array, (array.size,))

	def get_vector(self):
		goal, vector = find_nearest_goal(self.joint_pos, self.goals)
		factor = 1
		maxsize =  np.max(np.abs(vector[:-1]))
		if maxsize > 0.2*pi:
			factor = 0.2*pi / maxsize
		vector *= factor
		return vector

	def set_param_values(self, params):
		for key in params.keys():
			if hasattr(self, key):
				setattr(self, key, params[key])

	@property
	def action_space(self):
		bound = np.ones(N_JOINTS) * pi / 10
		return spaces.Box(-bound, bound)

	@property
	def observation_space(self):
		return spaces.Box(low=0, high=1, shape=(RESOLUTION * RESOLUTION * N_CHANNELS + N_JOINTS,))
	
	@property
	def joint_space(self):
		lbound = np.array([-10000, 42, 20, -10000, -10000, -10000])*pi/180
		ubound = np.array([10000, 318, 340, 10000, 10000, 10000])*pi/180
		return spaces.Box(lbound, ubound)
	
