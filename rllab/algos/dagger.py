import rllab.misc.logger as logger
from rllab.misc import ext, tensor_utils
from rllab.policies.base import Policy
from rllab.sampler import parallel_sampler
import numpy as np
import lasagne
import lasagne.layers as L
import lasagne.objectives as obj
import lasagne.regularization as R
import lasagne.updates as U
from rllab.algos.base import RLAlgorithm
from collections import deque

class DataPool(object):
    def __init__(self, max_size):
        self.observations = deque(maxlen=max_size)
        self.actions = deque(maxlen=max_size)

    def iterate_minibatches(self, batch_size=200, shuffle=True):
        indices = np.arange(len(self.actions))
        if shuffle:
            np.random.shuffle(indices)
        for start_idx in  range(0, indices.size, batch_size):
            excerpt = indices[start_idx:start_idx + batch_size]
            yield_obs = [self.observations[i] for i in excerpt]
            yield_act = [self.actions[i] for i in excerpt]
            yield yield_obs, yield_act

    def batch_add(self, obs_batch, act_batch):
        self.observations.extend(obs_batch)
        self.actions.extend(act_batch)

class Dagger(RLAlgorithm):
    """
    Dataset Aggregation
    """

    def __init__(
            self,
            env,
            policy,
            expert,
            n_itr=500,
            batch_size=200,
            max_path_length=25,
            max_pool_size=2000,
            training_epochs=10,
            training_batch_size=200,
            mixin_ratio=0.0,
            **kwargs):
        """
        :param env: Environment
        :param policy: Policy
        :param expert: Expert policy
        :param n_itr: number of iterations
        :param batch_size: Number of samples per iteration
        :param max_path_length: maximum number of steps in single trajectory
        :param mixin_ratio: ratio of expert decision to mix in with policy actions
        """
        assert batch_size >= training_batch_size
        self.env = env
        self.policy = policy
        self.expert = expert
        self.n_itr = n_itr
        self.batch_size = batch_size
        self.max_path_length = max_path_length
        self.dataset = DataPool(max_pool_size)
        self.training_epochs = training_epochs
        self.training_batch_size = training_batch_size
        self.mixin_ratio = mixin_ratio
        self.train_fn = None

    def start_worker(self):
        parallel_sampler.populate_task(self.env, self.policy)

    def init_opt(self):
        obs_var = self.policy._mean_network.input_layer.input_var

        act_var = self.env.action_space.new_tensor_variable(
            'act',
            extra_dims=1,
        )

        decision = L.get_output(self.policy._l_mean)
        loss = obj.squared_error(decision, act_var).mean()
        regularised_loss = loss + 1e-4*R.regularize_network_params(self.policy, R.l2)
        params = L.get_all_params(self.policy._l_mean, trainable=True)
        updates = U.adam(regularised_loss, params)
        self.train_fn = ext.compile_function(
            inputs=[obs_var, act_var],
            outputs=regularised_loss,
            updates=updates
        )


    def train(self):
        self.start_worker()
        self.init_opt()
        for itr in range(self.n_itr):
            with logger.prefix('itr #%d | ' % itr):
                paths = parallel_sampler.sample_paths(
                    policy_params=self.policy.get_param_values(),
                    max_samples=self.batch_size,
                    max_path_length=self.max_path_length,
                    env_params={'mixin_ratio': self.mixin_ratio**itr, 'evaluation': True},
                )
                observations = tensor_utils.concat_tensor_list([path["observations"] for path in paths])
                env_infos = tensor_utils.concat_tensor_dict_list([path["env_infos"] for path in paths])
                decisions = self.expert.get_actions(observations)[0]
                #decisions = env_infos['decision']
                self.dataset.batch_add(observations, decisions)
                #rewards = tensor_utils.concat_tensor_list([path["rewards"] for path in paths])
                if self.mixin_ratio > 1:
                    paths = paths = parallel_sampler.sample_paths(
                        policy_params=self.policy.get_param_values(),
                        max_samples=self.batch_size,
                        max_path_length=self.max_path_length,
                        env_params={'mixin_ratio' : 0.0,
                                    'evaluation' : True},
                    )
                self.optimize_policy(itr)
                undiscounted_returns = [sum(path["rewards"]) for path in paths]
                logger.record_tabular('Iteration', itr)
                logger.record_tabular('AverageReturn', np.mean(undiscounted_returns))
                logger.record_tabular('NumTrajs', len(paths))
                logger.record_tabular('MaxReturn', np.max(undiscounted_returns))
                logger.record_tabular('MinReturns', np.min(undiscounted_returns))
                logger.record_tabular('AllReturns', undiscounted_returns)
                self.log_diagnostics(paths)
                logger.log("saving snapshot...")
                params = self.get_itr_snapshot(itr)
                self.current_itr = itr + 1
                params["algo"] = self
                logger.save_itr_params(itr, params)
                logger.log("saved")
                logger.dump_tabular(with_prefix=False)

        self.shutdown_worker()

    def log_diagnostics(self, paths):
        self.env.log_diagnostics(paths)
        self.policy.log_diagnostics(paths)

    def get_itr_snapshot(self, itr):
        """
        Returns all the data that should be saved in the snapshot for this
        iteration.
        """
        return dict(
            env=self.env,
            itr=itr,
            policy=self.policy,
            expert=self.expert,
        )

    def optimize_policy(self, itr):
        ratio = self.dataset.actions.maxlen / len(self.dataset.actions)
        for epoch in range(int(ratio * self.training_epochs)):
            for minibatch in self.dataset.iterate_minibatches(self.training_batch_size):
                obs, act = minibatch
                loss = self.train_fn(obs, act)
