from rllab.algos.base import RLAlgorithm
from rllab.algos.components import DeterministicReplayPool
from rllab.sampler import parallel_sampler
from rllab.sampler.base import BaseSampler
import rllab.misc.logger as logger
from rllab.misc import tensor_utils
import rllab.plotter as plotter
from rllab.policies.base import Policy
import numpy as np
from rllab.envs.vrep.blr_supervised import load_data

class BatchSampler(BaseSampler):
    def start_worker(self):
        parallel_sampler.populate_task(self.algo.env, self.algo.policy, scope=self.algo.scope)

    def shutdown_worker(self):
        parallel_sampler.terminate_task(scope=self.algo.scope)

    def obtain_samples(self, itr, regularized=False):
        cur_params = self.algo.policy.get_param_values()
        paths = parallel_sampler.sample_paths(
            policy_params=cur_params,
            max_samples=self.algo.batch_size,
            max_path_length=self.algo.max_path_length,
            scope=self.algo.scope,
            regularized=regularized
        )
        if self.algo.whole_paths:
            return paths
        else:
            paths_truncated = parallel_sampler.truncate_paths(paths, self.algo.batch_size)
            return paths_truncated

class VisitationBatchSampler(BatchSampler):
    def __init__(self, algo, subsample_factor=1):
        self.algo = algo
        self.subsample_factor = subsample_factor

    def process_samples(self, itr, paths):
            
        #update the visitation BLR and add intrinsic rewards to the reward signal
        policy = self.algo.policy
        observations = tensor_utils.concat_tensor_list([path["observations"] for path in paths])
        #do subsampling if necessary
        n_samples = self.algo.batch_size
        indices = np.arange(n_samples)
        if self.subsample_factor < 1:
            indices = np.random.choice(
                n_samples, int(n_samples * self.subsample_factor), replace=False)
        paths[0]['indices'] = indices
        phis = policy.get_vis_phis(observations)
        intr_rew = policy.get_intrinsic_phi_reward(phis)
        i = 0
        for path in paths:
            path["intr_rewards"] = []
            for j in range(len(path["rewards"])):
                path["intr_rewards"].append(intr_rew[i])
                path["rewards"][j] += intr_rew[i]
                i += 1
        policy.vis_phi_batch_update(phis)

        return super(VisitationBatchSampler, self).process_samples(itr, paths) 


class RNDBatchSampler(BatchSampler):
    def __init__(self, algo, subsample_factor=1):
        self.algo = algo
        self.subsample_factor = subsample_factor

    def process_samples(self, itr, paths):
            
        #update the predictor network and add intrinsic rewards to the reward signal
        policy = self.algo.policy
        observations = tensor_utils.concat_tensor_list([path["observations"] for path in paths])
        #do subsampling if necessary
        n_samples = self.algo.batch_size
        indices = np.arange(n_samples)
        if self.subsample_factor < 1:
            indices = np.random.choice(
                n_samples, int(n_samples * self.subsample_factor), replace=False)
        paths[0]['indices'] = indices
        rewards = policy.get_intrinsic_reward(observations)
        raw_rew, intr_rew = rewards['raw_int_rew'], rewards['norm_int_rew']
        norm_rew = policy.normalize_reward(intr_rew)
        i = 0
        for path in paths:
            path["intr_rewards"] = []
            for j in range(len(path["rewards"])):
                path["intr_rewards"].append(intr_rew[i])
                path["rewards"][j] += intr_rew[i]
                i += 1
        policy.update_predictor(observations)
        policy.update_normalization(raw_rew)

        return super(RNDBatchSampler, self).process_samples(itr, paths) 


class BatchPolopt(RLAlgorithm):
    """
    Base class for batch sampling-based policy optimization methods.
    This includes various policy gradient methods like vpg, npg, ppo, trpo, etc.
    """

    def __init__(
            self,
            env,
            policy,
            baseline,
            scope=None,
            n_itr=500,
            start_itr=0,
            batch_size=5000,
            max_path_length=500,
            discount=0.99,
            gae_lambda=1,
            plot=False,
            pause_for_plot=False,
            center_adv=True,
            positive_adv=False,
            store_paths=False,
            whole_paths=True,
            sampler_cls=None,
            sampler_args=None,
            regularized=False,
            max_pool_size=4000,
            resample_gap=50,
            **kwargs
    ):
        """
        :param env: Environment
        :param policy: Policy
        :type policy: Policy
        :param baseline: Baseline
        :param scope: Scope for identifying the algorithm. Must be specified if running multiple algorithms
        simultaneously, each using different environments and policies
        :param n_itr: Number of iterations.
        :param start_itr: Starting iteration.
        :param batch_size: Number of samples per iteration.
        :param max_path_length: Maximum length of a single rollout.
        :param discount: Discount.
        :param gae_lambda: Lambda used for generalized advantage estimation.
        :param plot: Plot evaluation run after each iteration.
        :param pause_for_plot: Whether to pause before contiuing when plotting.
        :param center_adv: Whether to rescale the advantages so that they have mean 0 and standard deviation 1.
        :param positive_adv: Whether to shift the advantages so that they are always positive. When used in
        conjunction with center_adv the advantages will be standardized before shifting.
        :param store_paths: Whether to save all paths data to the snapshot.
        :param max_pool_size: The maximum number of old samples to keep in a replay pool
        :param resample_gap: How many iterations to wait before resampling for bayesian regression
        """
        self.env = env
        self.policy = policy
        self.baseline = baseline
        self.scope = scope
        self.n_itr = n_itr
        self.current_itr = start_itr
        self.batch_size = batch_size
        self.max_path_length = max_path_length
        self.discount = discount
        self.gae_lambda = gae_lambda
        self.plot = plot
        self.pause_for_plot = pause_for_plot
        self.center_adv = center_adv
        self.positive_adv = positive_adv
        self.store_paths = store_paths
        self.whole_paths = whole_paths
        if sampler_cls is None:
            sampler_cls = BatchSampler
        if sampler_args is None:
            sampler_args = dict()
        self.sampler = sampler_cls(self, **sampler_args)
        self.regularized = regularized
        self.max_pool_size = max_pool_size
        self.resample_gap = resample_gap

    def start_worker(self):
        self.sampler.start_worker()
        if self.plot:
            plotter.init_plot(self.env, self.policy)

    def shutdown_worker(self):
        self.sampler.shutdown_worker()

    def train(self):
        is_bayesian = self.policy.bayesian
        if is_bayesian:
            pool = DeterministicReplayPool(
                max_pool_size=self.max_pool_size,
                observation_dim=self.env.observation_space.flat_dim,
                action_dim=self.env.action_space.flat_dim,
            )
            X,y = load_data("/rllab/rllab/envs/vrep/data/half/demo_")
            self.demo = dict(X=X, y=y)
        self.start_worker()
        self.init_opt()
        for itr in range(self.current_itr, self.n_itr):
            with logger.prefix('itr #%d | ' % itr):
                paths = self.sampler.obtain_samples(itr, self.regularized)
                if 'alpha' in self.env.__dict__: self.env.evolve_alpha()
                samples_data = self.sampler.process_samples(itr, paths)
                if is_bayesian:
                    successful_paths = self.sampler.find_successful_paths(paths)
                    obs = successful_paths["observations"]
                    act = successful_paths["actions"]
                    pool.add_samples(obs, act, [0] * len(act), [False] * len(act))
                self.log_diagnostics(paths)
                self.optimize_policy(itr, samples_data)
                if is_bayesian:
                    self.policy._regression.reset_params()
                    self.policy._f_blr_update(X[:4000], y[:4000])
                logger.log("saving snapshot...")
                params = self.get_itr_snapshot(itr, samples_data)
                self.current_itr = itr + 1
                params["algo"] = self
                if self.store_paths:
                    params["paths"] = samples_data["paths"]
                logger.save_itr_params(itr, params)
                logger.log("saved")
                logger.dump_tabular(with_prefix=False)
                if self.plot:
                    self.update_plot()
                    if self.pause_for_plot:
                        input("Plotting evaluation run: Press Enter to "
                                  "continue...")
                #if is_bayesian and itr % self.resample_gap == 0 and itr != 0:
                    #TODO: re-compute the weights of the bayesian linear regression in the policy
                    #successful_paths = pool.get_batch()
                    #obs = successful_paths["observations"].astype(np.float32)
                    #act = successful_paths["actions"].astype(np.float32)
                    #phi = self.policy.get_phis(obs)
                    #self.policy._regression.learn_all_params(phi, act)

        self.shutdown_worker()

    def log_diagnostics(self, paths):
        self.env.log_diagnostics(paths)
        self.policy.log_diagnostics(paths)
        self.baseline.log_diagnostics(paths)

    def init_opt(self):
        """
        Initialize the optimization procedure. If using theano / cgt, this may
        include declaring all the variables and compiling functions
        """
        raise NotImplementedError

    def get_itr_snapshot(self, itr, samples_data):
        """
        Returns all the data that should be saved in the snapshot for this
        iteration.
        """
        raise NotImplementedError

    def optimize_policy(self, itr, samples_data):
        raise NotImplementedError

    def update_plot(self):
        if self.plot:
            plotter.update_plot(self.policy, self.max_path_length)
