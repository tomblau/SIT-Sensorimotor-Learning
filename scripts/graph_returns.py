import numpy as np
import matplotlib.pyplot as plt
import itertools

def get_returns(root, exp_id):
	print("%s/%s/series.npz" %(root, exp_id))
	rets = np.load("%s/%s/series.npz" %(root, exp_id))['allrets']
	minlen = min([len(ret) for ret in rets])
	truncrets = [[ret[i] for ret in rets] for i in range(minlen)]
	con = np.concatenate([[x for x in
		itertools.chain.from_iterable(
			itertools.zip_longest(*truncrets[i])) if x]
		for i in range(len(truncrets))])
	return con

def get_iqr(rets):
	meds = [np.median(rets[i:i+100]) for i in range(len(rets)-100)]
	uq = [np.percentile(rets[i:i+100], 75) for i in range(len(rets)-100)]
	lq = [np.percentile(rets[i:i+100], 25) for i in range(len(rets)-100)]
	return meds, uq, lq

def get_ci(rets):
	means = np.array([np.mean(rets[i:i+100]) for i in range(len(rets)-100)])
	stds = np.array([np.std(rets[i:i+100]) for i in range(len(rets)-100)])
	return means, stds

if __name__=="__main__":
	exp_folder="/home/tom/rllab/data/local/experiment"
	graph_folder="/home/tom/rllab/data/local/experiment/graphs"
	experiments = [
		"experiment_2018_12_16_00_51_58_0001",
		"experiment_2018_12_12_00_05_40_0001",
	]
	colors = ['#FF0000', '#0000FF', '#00FF00', '#FFA500', '#999900', '#990099']
	labels = ['Curiosity', 'TRPO']
	trendline = []
	interval = []
	mode='ci'
	fig = plt.figure(1)
	fig.clear()
	ax = fig.add_subplot(111, )
	i = 0
	for exp_id in experiments:
		rets = get_returns(exp_folder, exp_id)
		if mode == 'iqr':
			meds, uq, lq = get_iqr(rets)
			trendline = meds
			interval = {'ub' : uq, 'lb' : lq}
		elif mode == 'ci':
			means, stds = get_ci(rets)
			trendline = means
			interval = {'ub' : means + stds, 'lb' : means - stds}
		indices = np.arange(len(trendline))
		ax.plot(indices, trendline,
			label=labels[i], color=colors[i], linewidth=0.5)
		plt.fill_between(indices, interval['ub'], interval['lb'],
			color=colors[i], alpha=0.25, linewidth=0.25)
		i += 1
	handles, labels = ax.get_legend_handles_labels()
	lgd = ax.legend(handles, labels, fontsize='large', loc='upper center', bbox_to_anchor=(0.5, -0.15), ncol=2)
	plt.grid('on')
	plt.show()
