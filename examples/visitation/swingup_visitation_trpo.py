from rllab.misc.instrument import run_experiment_lite

seed=346820
def run_task(*_):
    from rllab.algos.trpo import TRPO
    from rllab.envs.box2d.cartpole_swingup_env import CartpoleSwingupEnv
    from rllab.envs.normalized_env import normalize
    from rllab.algos.batch_polopt import VisitationBatchSampler
    import joblib

    env = normalize(CartpoleSwingupEnv(), normalize_obs=True)
    
    init_weights="/rllab/data/local/experiment/networks/VisitationIdentSwingupPolicy1.pkl"
    data = joblib.load(init_weights)
    policy = data["policy"]
    baseline = data["baseline"]
    
    algo = TRPO(
        env=env,
        policy=policy,
        baseline=baseline,
        batch_size=40*env.horizon,
        max_path_length=env.horizon,
        n_itr=1010,
        discount=0.99,
        step_size=0.01,
        sampler_cls=VisitationBatchSampler,
    )
    algo.train()

run_experiment_lite(
    run_task,
    n_parallel=1,
    snapshot_mode="gap",
    snapshot_gap=1000,
    seed=seed,
)
