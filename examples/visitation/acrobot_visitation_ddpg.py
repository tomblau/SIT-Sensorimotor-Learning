from rllab.misc.instrument import run_experiment_lite

seed=1
def run_task(*_):
    from rllab.algos.ddpg import DDPG
    from rllab.envs.acrobot_env import AcrobotEnv
    from rllab.envs.normalized_env import normalize
    from rllab.algos.batch_polopt import VisitationBatchSampler
    import joblib


    env = normalize(AcrobotEnv(seed=seed))#, normalize_obs=True)
    
    init_weights="/rllab/data/local/experiment/networks/VisitationDDPGAcrobotPolicy1.pkl"
    data = joblib.load(init_weights)
    policy = data["policy"]
    es = data["es"]
    qf = data["qf"]
    

    algo = DDPG(
        env=env,
        policy=policy,
        es=es,
        qf=qf,
        batch_size=100,
        max_path_length=env.horizon,
        epoch_length=4*env.horizon,
        min_pool_size=10000,
        n_epochs=1010,
        discount=0.99,
        scale_reward=0.01,
        qf_learning_rate=1e-3,
        policy_learning_rate=1e-4,
        # Uncomment both lines (this and the plot parameter below) to enable plotting
        # plot=True,
    )
    algo.train()

run_experiment_lite(
    run_task,
    n_parallel=1,
    snapshot_mode="gap",
    snapshot_gap=1000,
    seed=seed,
)
