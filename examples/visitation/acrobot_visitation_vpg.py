from rllab.misc.instrument import run_experiment_lite


seed = 109126
def run_task(*_):
    from rllab.algos.vpg import VPG
    from rllab.envs.acrobot_env import AcrobotEnv
    from rllab.envs.normalized_env import normalize
    from rllab.algos.batch_polopt import VisitationBatchSampler
    import joblib


    env = normalize(AcrobotEnv(seed=seed), normalize_obs=True)
    
    init_weights="/rllab/data/local/experiment/networks/VisitationAcrobotPolicy1.pkl"
    data = joblib.load(init_weights)
    policy = data["policy"]
    baseline = data["baseline"]
    
    algo = VPG(
        env=env,
        policy=policy,
        baseline=baseline,
        batch_size=4*env.horizon,
        max_path_length=env.horizon,
        n_itr=501,
        discount=0.99,
        step_size=0.01,
        sampler_cls=VisitationBatchSampler,
    )
    algo.train()

run_experiment_lite(
    run_task,
    n_parallel=1,
    snapshot_mode="gap",
    snapshot_gap=500,
    seed=seed,
)
