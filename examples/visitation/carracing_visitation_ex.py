from rllab.misc.instrument import run_experiment_lite


def run_task(*_):
    from rllab.envs.car_racing import CarRacingEnv
    from rllab.envs.normalized_env import normalize
    from rllab.algos.batch_polopt import VisitationBatchSampler
    from rllab.algos.trpo import TRPO
    import joblib

    env = CarRacingEnv()

    action_dim = env.action_space.flat_dim
    obs_shape = env.observation_space.shape
    subsample_factor = 0.25

    init_weights="/rllab/data/local/experiment/networks/VisitationCarracingPolicy1.pkl"
    data = joblib.load(init_weights)
    policy = data["policy"]
    baseline = data["baseline"]

    sampler_args = {
        "subsample_factor" : subsample_factor
    }

    algo = TRPO(
        env=env,
        policy=policy,
        baseline=baseline,
        batch_size=4*env.horizon,
        max_path_length=env.horizon,
        n_itr=4001,
        discount=0.99,
        step_size=0.01,
        sampler_cls=VisitationBatchSampler,
        sampler_args=sampler_args,
    )
    algo.train()


run_experiment_lite(
    run_task,
    # Number of parallel workers for sampling
    n_parallel=1,
    # Only keep the snapshot parameters for the last iteration
    snapshot_mode="gap",
    snapshot_gap=50,
)
