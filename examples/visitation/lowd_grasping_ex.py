from __future__ import print_function
from __future__ import absolute_import

from rllab.misc.instrument import stub, run_experiment_lite

seeds = [975546, 516016, 191858, 728967, 400057, 272061, 166820, 360305, 53825, 243145]
exp_id = 5
seed = seeds[exp_id - 1]

def run_task(*_):
    from rllab.algos.trpo import TRPO
    from rllab.algos.batch_polopt import VisitationBatchSampler
    from rllab.envs.vrep.grasping.jaco_env import LowDJacoEnv
    import joblib
    
    env = LowDJacoEnv()
    init_weights="/rllab/data/local/experiment/networks/VisitationLowDGraspingPolicy%d.pkl" %exp_id
    data = joblib.load(init_weights)
    policy = data["policy"]
    baseline = data["baseline"]

    algo = TRPO(
        env=env,
        policy=policy,
        baseline=baseline,
        batch_size=100,
        max_path_length=25,
        n_itr=10010,
        discount=0.99,
        step_size=0.01,
        sampler_cls=VisitationBatchSampler,
        # Uncomment both lines (this and the plot parameter below) to enable plotting
        # plot=True,
    )
    algo.train()

run_experiment_lite(
    run_task,
    # Number of parallel workers for sampling
    n_parallel=1,
    seed=seed,
    # Only keep the snapshot parameters for the last iteration
    snapshot_mode="gap",
    snapshot_gap=50,
    # Specifies the seed for the experiment. If this is not provided, a random seed
    # will be used
    # plot=True,
)
