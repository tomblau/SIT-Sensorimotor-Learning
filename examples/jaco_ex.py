from __future__ import print_function
from __future__ import absolute_import

from rllab.misc.instrument import stub, run_experiment_lite


seeds = [49445, 424522, 984189, 922977, 35796, 947600, 801314, 624999, 259907, 658686]
exp_id = 1
seed = seeds[exp_id - 1]

def run_task(*_):
    from rllab.algos.trpo import TRPO
    from rllab.baselines.gaussian_mlp_baseline import GaussianMLPBaseline
    from rllab.envs.vrep.jaco_env import JacoEnv
    from rllab.envs.normalized_env import normalize
    from rllab.policies.gaussian_mlp_policy import GaussianMLPPolicy
    import lasagne.nonlinearities as NL
    from rllab.core.network import AltRoboNetwork as RoboNetwork
    from rllab.core.lasagne_helpers import ScaledSigmoidPlus
    import joblib
    
    env = JacoEnv()
    action_dim = env.action_space.flat_dim
    obs_shape = (128, 128, 4)
    init_weights = None
    #init_weights="/rllab/data/local/experiment/networks/trained_l2_reach%d.pkl" %exp_id
    
    if init_weights:
        data = joblib.load(init_weights)
        policy = data["policy"]
        baseline = data["baseline"]
    else:
        mean_network = RoboNetwork(
                         input_shape=obs_shape,
                         output_dim=action_dim,
                         hidden_sizes=(256, 256, 64, 16, 5),
                         pool_sizes=(2, 2, 2, 2),
                         conv_filters=(32, 64, 64, 64),
                         conv_filter_sizes=(5, 5, 3, 3),
                         conv_strides=(2, 2, 1, 1),
                         conv_pads=(2, 2, 1, 1),
                         hidden_nonlinearity=NL.rectify,
                         output_nonlinearity=NL.identity,
                    )
    
        std_network = RoboNetwork(
                        input_shape=obs_shape,
                        input_layer=mean_network.input_layer,
                        output_dim=action_dim,
                        hidden_sizes=(256, 256, 64),
                        pool_sizes=(2, 2, 2, 2),
                        conv_filters=(32, 64, 64, 64),
                        conv_filter_sizes=(5, 5, 3, 3),
                        conv_strides=(2, 2, 1, 1),
                        conv_pads=(2, 2, 1, 1),
                        hidden_nonlinearity=NL.rectify,
                        output_nonlinearity=ScaledSigmoidPlus(scale_out=-5),
                    )
    
        baseline_network = RoboNetwork(
                    input_shape=obs_shape,
                    output_dim=1,
                    hidden_sizes=(256, 256, 64),
                    pool_sizes=(2, 2, 2, 2),
                    conv_filters=(32, 64, 64, 64),
                    conv_filter_sizes=(5, 5, 3, 3),
                    conv_strides=(2, 2, 1, 1),
                    conv_pads=(2, 2, 1, 1),
                    hidden_nonlinearity=NL.rectify,
                    output_nonlinearity=NL.tanh,
                )
    
        baseline_std = RoboNetwork(
                    input_shape=obs_shape,
                    input_layer=baseline_network.input_layer,
                    output_dim=1,
                    hidden_sizes=(256, 256, 64),
                    pool_sizes=(2, 2, 2, 2),
                    conv_filters=(32, 64, 64, 64),
                    conv_filter_sizes=(5, 5, 3, 3),
                    conv_strides=(2, 2, 1, 1),
                    conv_pads=(2, 2, 1, 1),
                    hidden_nonlinearity=NL.rectify,
                    output_nonlinearity=NL.tanh,
                )
    
        policy = GaussianMLPPolicy(
                   env_spec=env.spec,
                   mean_network=mean_network,
                   std_network=std_network,
                   #init_std=0.5
                )
    
        reg_args = {
            "mean_network" : baseline_network,
            "std_network" : baseline_std,
            #"adaptive_std" : True,
        }
    
        baseline = GaussianMLPBaseline(
                     env_spec=env.spec,
                     regressor_args=reg_args
                  )
    
    
    algo = TRPO(
        env=env,
        policy=policy,
        baseline=baseline,
        batch_size=100,
        max_path_length=25,
        n_itr=4010,
        discount=0.99,
        step_size=0.01,
        # Uncomment both lines (this and the plot parameter below) to enable plotting
        # plot=True,
    )
    algo.train()

run_experiment_lite(
    run_task,
    # Number of parallel workers for sampling
    n_parallel=1,
    seed=seed,
    # Only keep the snapshot parameters for the last iteration
    snapshot_mode="gap",
    snapshot_gap=50,
    # Specifies the seed for the experiment. If this is not provided, a random seed
    # will be used
    # plot=True,
)
