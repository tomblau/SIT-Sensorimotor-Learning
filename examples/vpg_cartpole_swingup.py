from rllab.misc.instrument import run_experiment_lite


def run_task(*_):
    from rllab.algos.vpg import VPG
    from rllab.envs.box2d.cartpole_swingup_env import CartpoleSwingupEnv
    from rllab.envs.normalized_env import normalize
    from rllab.algos.batch_polopt import VisitationBatchSampler
    import joblib

    
    env = normalize(CartpoleSwingupEnv())
    
    init_weights="/rllab/data/local/experiment/networks/VisitationVPGSwingupPolicy.pkl"
    data = joblib.load(init_weights)
    policy = data["policy"]
    baseline = data["baseline"]
    
    algo = VPG(
        env=env,
        policy=policy,
        baseline=baseline,
        batch_size=4000,
        max_path_length=100,
        n_itr=1010,
        discount=0.99,
        step_size=0.01,
        sampler_cls=VisitationBatchSampler,
    )
    algo.train()

run_experiment_lite(
    run_task,
    n_parallel=1,
    snapshot_mode="gap",
    snapshot_gap=1000,
    seed=3,
)
