from __future__ import print_function
from __future__ import absolute_import

from rllab.algos.dagger import Dagger
from rllab.envs.vrep.expert_controller import ExpertController
from rllab.envs.vrep.controlled_env import ControlledEnv
from rllab.envs.normalized_env import normalize
from rllab.misc.instrument import stub, run_experiment_lite
from rllab.policies.gaussian_mlp_policy import GaussianMLPPolicy
import lasagne.nonlinearities as NL
from rllab.core.network import AltRoboNetwork as RoboNetwork
import joblib


def run_task(*_):
    env = normalize(ControlledEnv())
    expert = None
    action_dim = env.action_space.flat_dim
    obs_shape = (128, 128, 4)
    init_weights = None
    init_weights="/rllab/data/local/experiment/experiment_2017_06_23_00_42_45_0001/near_deterministic.pkl"
    expert_weights="/rllab/data/local/experiment/experiment_2017_06_16_01_03_17_0001/itr_4000.pkl"
    
    data = joblib.load(expert_weights)
    expert = data['policy']

    if init_weights:
        data = joblib.load(init_weights)
        policy = data["policy"]
        #baseline = data["baseline"]
    else:
        mean_network = RoboNetwork(
                         input_shape=obs_shape,
                         output_dim=action_dim,
                         hidden_sizes=(256, 256, 64),
                         pool_sizes=(2, 2, 2),
                         conv_filters=(32, 64, 64),
                         conv_filter_sizes=(6, 6, 4),
                         conv_strides=(2, 2, 2),
                         conv_pads=(1, 1, 1),
                         hidden_nonlinearity=NL.tanh,
                         output_nonlinearity=NL.tanh,
                    )
    
        std_network = RoboNetwork(
                        input_shape=obs_shape,
                        input_layer=mean_network.input_layer,
                        output_dim=action_dim,
                        hidden_sizes=(1024, 1024, 256),
                        pool_sizes=(2, 2, 2),
                        conv_filters=(32, 64, 64),
                        conv_filter_sizes=(6, 6, 4),
                        conv_strides=(2, 2, 2),
                        conv_pads=(1, 1, 1),
                        hidden_nonlinearity=NL.tanh,
                        output_nonlinearity=NL.tanh,
                    )
    
    
        policy = GaussianMLPPolicy(
                   env_spec=env.spec,
                   mean_network=mean_network,
                   std_network=std_network
                )
    
    algo = Dagger(
        env=env,
        policy=policy,
        expert=expert,
        batch_size=200,
        training_batch_size=200,
        max_path_length=25,
        max_pool_size=10000,
        mixin_ratio=0.0,
        n_itr=1001,
        discount=0.99,
        # Uncomment both lines (this and the plot parameter below) to enable plotting
        # plot=True,
    )
    algo.train()

run_experiment_lite(
    run_task,
    # Number of parallel workers for sampling
    n_parallel=1,
    # Only keep the snapshot parameters for the last iteration
    snapshot_mode="last",
    # Specifies the seed for the experiment. If this is not provided, a random seed
    # will be used
    seed=1,
    # plot=True,
)
