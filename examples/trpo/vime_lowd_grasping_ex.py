from rllab.misc.instrument import stub, run_experiment_lite
from rllab.envs.vrep.grasping.jaco_env import LowDJacoEnv
from rllab.policies.gaussian_mlp_policy import GaussianMLPPolicy
from rllab.baselines.gaussian_mlp_baseline import GaussianMLPBaseline
import lasagne.nonlinearities as NL
from rllab.core.network import MLP
from rllab.core.lasagne_helpers import ScaledSigmoidPlus
from sandbox.vime.algos.trpo_expl import TRPO
import numpy as np

stub(globals())

seeds = [975546, 516016, 191858, 728967, 400057, 272061, 166820, 360305, 53825, 243145]
exp_id = 5
seed = seeds[exp_id - 1]
eta = 0.0001

env = LowDJacoEnv()
action_dim = env.action_space.flat_dim
obs_shape = env.observation_space.shape

mean_network = MLP(
    input_shape=obs_shape,
    output_dim=action_dim,
    hidden_sizes=(256, 256, 64, 16, 5),
    hidden_nonlinearity=NL.rectify,
    output_nonlinearity=NL.identity,
)
std_network = MLP(
    input_shape=obs_shape,
    input_layer=mean_network.input_layer,
    output_dim=action_dim,
    hidden_sizes=(256, 256, 64),
    hidden_nonlinearity=NL.rectify,
    output_nonlinearity=ScaledSigmoidPlus(scale_out=-5),
)
baseline_network = MLP(
    input_shape=obs_shape,
    output_dim=1,
    hidden_sizes=(256, 256, 64),
    hidden_nonlinearity=NL.rectify,
    output_nonlinearity=NL.tanh,
)
baseline_std = MLP(
    input_shape=obs_shape,
    input_layer=baseline_network.input_layer,
    output_dim=1,
    hidden_sizes=(256, 256, 64),
    hidden_nonlinearity=NL.rectify,
    output_nonlinearity=NL.tanh,
)
policy = GaussianMLPPolicy(
    env_spec=env.spec,
    mean_network=mean_network,
    std_network=std_network,
)
reg_args = {
    "mean_network" : baseline_network,
    "std_network" : baseline_std,
}
baseline = GaussianMLPBaseline(
                env_spec=env.spec,
                regressor_args=reg_args
            )


algo = TRPO(
    env=env,
    policy=policy,
    baseline=baseline,
    init_fp="/rllab/data/local/experiment/networks/grasping_params%d.npz" %exp_id,
    batch_size=100,
    whole_paths=True,
    max_path_length=25,
    n_itr=10010,
    step_size=0.01,
    eta=eta,
    snn_n_samples=10,
    subsample_factor=1.0,
    use_replay_pool=True,
    use_kl_ratio=True,
    use_kl_ratio_q=True,
    n_itr_update=1,
    kl_batch_size=1,
    normalize_reward=False,
    replay_pool_size=1000000,
    n_updates_per_sample=5000,
    second_order_update=True,
    unn_n_hidden=[32],
    unn_layers_type=[1, 1],
    unn_learning_rate=0.0001
)

run_experiment_lite(
    algo.train(),
    exp_prefix="trpo-expl",
    n_parallel=1,
    snapshot_mode="gap",
    snapshot_gap=100,
    seed=seed,
    mode="local",
    script="sandbox/vime/experiments/run_experiment_lite.py",
)
