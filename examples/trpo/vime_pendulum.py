from rllab.misc.instrument import stub, run_experiment_lite
from rllab.baselines.linear_feature_baseline import LinearFeatureBaseline
from rllab.envs.pendulum_env import PendulumEnv
from rllab.policies.gaussian_mlp_policy import GaussianMLPPolicy
from rllab.envs.normalized_env import NormalizedEnv
import lasagne.layers as L
from sandbox.vime.algos.trpo_expl import TRPO
import joblib
import numpy as np

stub(globals())

seeds = [661643, 14495, 118501, 970775, 780238, 977055, 647323, 328208, 960850, 899519]
exp_id = 2
seed = seeds[exp_id - 1]
eta = 0.0001

env = NormalizedEnv(PendulumEnv(seed=seed), normalize_obs=True)
init_filepath = None

policy = GaussianMLPPolicy(
    env_spec=env.spec,
    hidden_sizes=(32, 32),
    init_std=0.3
)

baseline = LinearFeatureBaseline(
    env_spec=env.spec,
)

algo = TRPO(
    env=env,
    policy=policy,
    baseline=baseline,
    batch_size=40*env.horizon,
    whole_paths=True,
    max_path_length=env.horizon,
    n_itr=1010,
    step_size=0.01,
    eta=eta,
    snn_n_samples=10,
    subsample_factor=1.0,
    use_replay_pool=True,
    use_kl_ratio=True,
    use_kl_ratio_q=True,
    n_itr_update=1,
    kl_batch_size=1,
    normalize_reward=False,
    replay_pool_size=1000000,
    n_updates_per_sample=5000,
    second_order_update=True,
    unn_n_hidden=[32],
    unn_layers_type=[1, 1],
    unn_learning_rate=0.0001
)

run_experiment_lite(
    algo.train(),
    exp_prefix="trpo-expl",
    n_parallel=1,
    snapshot_mode="gap",
    snapshot_gap=100,
    seed=seed,
    mode="local",
    script="sandbox/vime/experiments/run_experiment_lite.py",
)
