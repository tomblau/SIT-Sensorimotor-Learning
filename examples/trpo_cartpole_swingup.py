from rllab.misc.instrument import run_experiment_lite


def run_task(*_):
    from rllab.algos.trpo import TRPO
    from rllab.baselines.linear_feature_baseline import LinearFeatureBaseline
    from rllab.envs.box2d.cartpole_swingup_env import CartpoleSwingupEnv
    from rllab.envs.normalized_env import normalize
    from rllab.policies.gaussian_mlp_policy import GaussianMLPPolicy
    
    env = normalize(CartpoleSwingupEnv(), normalize_obs=False)
    init_weights=None
    init_weights="/rllab/data/local/experiment/experiment_2019_02_05_02_44_55_0001/itr_1000.pkl"
    if init_weights:
        data = joblib.load(init_weights)
        policy = data["policy"]
        baseline = data["baseline"]
    else:
        policy = GaussianMLPPolicy(
            env_spec=env.spec,
            # The neural network policy should have two hidden layers, each with 32 hidden units.
            hidden_sizes=(32, 32),
            init_std=0.3,
        )
        
        baseline = LinearFeatureBaseline(env_spec=env.spec)
    
    algo = TRPO(
        env=env,
        policy=policy,
        baseline=baseline,
        batch_size=80*env.horizon,
        max_path_length=env.horizon,
        n_itr=1,
        discount=0.99,
        step_size=0.01,
        
    )
    algo.train()

run_experiment_lite(
    run_task,
    n_parallel=1,
    snapshot_mode="gap",
    snapshot_gap=1000,
    seed=1,
)
