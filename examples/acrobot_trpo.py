from rllab.misc.instrument import run_experiment_lite


def run_task(*_):
    from rllab.algos.trpo import TRPO
    from rllab.baselines.linear_feature_baseline import LinearFeatureBaseline
    from rllab.envs.gym_env import GymEnv
    from rllab.envs.normalized_env import normalize
    from rllab.envs.acrobot_env import AcrobotEnv
    from rllab.policies.gaussian_mlp_policy import GaussianMLPPolicy
    # Please note that different environments with different action spaces may require different
    # policies. For example with a Box action space, a GaussianMLPPolicy works, but for a Discrete
    # action space may need to use a CategoricalMLPPolicy (see the trpo_gym_cartpole.py example)
    env = normalize(AcrobotEnv())

    policy = GaussianMLPPolicy(
        env_spec=env.spec,
        # The neural network policy should have two hidden layers, each with 32 hidden units.
        hidden_sizes=(32, 32),
        init_std=0.1,
    )

    baseline = LinearFeatureBaseline(env_spec=env.spec)

    print(env.horizon)
    algo = TRPO(
        env=env,
        policy=policy,
        baseline=baseline,
        batch_size=4000,
        max_path_length=env.horizon,
        n_itr=500,
        discount=0.99,
        step_size=0.01,
    )
    algo.train()


run_experiment_lite(
    run_task,
    # Number of parallel workers for sampling
    n_parallel=1,
    snapshot_mode="gap",
    snapshot_gap=50,
    # Specifies the seed for the experiment. If this is not provided, a random seed
    # will be used
    seed=1,
)
