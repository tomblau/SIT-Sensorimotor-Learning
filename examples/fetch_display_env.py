from __future__ import print_function
from __future__ import absolute_import

from rllab.misc.instrument import stub, run_experiment_lite


def run_task(*_):
    from rllab.algos.trpo import TRPO
    from rllab.baselines.gaussian_mlp_baseline import GaussianMLPBaseline
    from rllab.envs.robotics.fetch.visual_pick_and_place import VisualFetchPickAndPlaceEnv
    from rllab.envs.normalized_env import normalize
    from rllab.policies.gaussian_mlp_policy import GaussianMLPPolicy
    import lasagne.nonlinearities as NL
    from rllab.core.network import AltRoboNetwork as RoboNetwork
    from rllab.core.lasagne_helpers import ScaledSigmoidPlus
    import joblib
    
    env = VisualFetchPickAndPlaceEnv()
    action_dim = env.action_space.flat_dim
    obs_shape = (256, 256, 4)
    init_weights="/home/tom/rllab/data/local/experiment/networks/display_fetch_pickandplace.pkl"
    
    data = joblib.load(init_weights)
    policy = data["policy"]
    
    
    algo = TRPO(
        env=env,
        policy=policy,
        baseline=None,
        batch_size=200,
        max_path_length=25,
        n_itr=4000,
        discount=0.99,
        step_size=0.01,
        # Uncomment both lines (this and the plot parameter below) to enable plotting
        # plot=True,
    )
    algo.train()

run_experiment_lite(
    run_task,
    # Number of parallel workers for sampling
    n_parallel=1,
    # Only keep the snapshot parameters for the last iteration
    snapshot_mode="gap",
    snapshot_gap=50,
    # Specifies the seed for the experiment. If this is not provided, a random seed
    # will be used
    # plot=True,
)
