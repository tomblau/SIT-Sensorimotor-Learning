from rllab.misc.instrument import run_experiment_lite


def run_task(*_):
    from rllab.envs.car_racing import CarRacingEnv
    from rllab.envs.normalized_env import normalize
    from rllab.baselines.gaussian_mlp_baseline import GaussianMLPBaseline
    from rllab.policies.gaussian_mlp_policy import GaussianMLPPolicy
    from rllab.core.lasagne_helpers import ScaledSigmoidPlus
    from rllab.core.network import ConvNetwork
    from rllab.algos.trpo import TRPO
    import lasagne.nonlinearities as NL
    import joblib

    env = CarRacingEnv(human=False)

    action_dim = env.action_space.flat_dim
    obs_shape = env.observation_space.shape
    init_weights = None
    init_weights="/home/tom/rllab/data/local/experiment/networks/display_carracing1.pkl"
    
    if init_weights:
        data = joblib.load(init_weights)
        policy = data["policy"]
        baseline = data["baseline"]
    else:
        mean_network = ConvNetwork(
                         input_shape=obs_shape,
                         output_dim=action_dim,
                         hidden_sizes=(256, 256, 64),
                         pool_sizes=(2, 2, 2, 2),
                         conv_filters=(32, 64, 64, 64),
                         conv_filter_sizes=(5, 5, 3, 3),
                         conv_strides=(2, 2, 1, 1),
                         conv_pads=(2, 2, 1, 1),
                         hidden_nonlinearity=NL.rectify,
                         output_nonlinearity=NL.identity,
                    )
    
        std_network = ConvNetwork(
                        input_shape=obs_shape,
                        input_layer=mean_network.input_layer,
                        output_dim=action_dim,
                        hidden_sizes=(256, 256, 64),
                        pool_sizes=(2, 2, 2, 2),
                        conv_filters=(32, 64, 64, 64),
                        conv_filter_sizes=(5, 5, 3, 3),
                        conv_strides=(2, 2, 1, 1),
                        conv_pads=(2, 2, 1, 1),
                        hidden_nonlinearity=NL.rectify,
                        output_nonlinearity=ScaledSigmoidPlus(scale_out=-2.5),
                    )
    
        baseline_network = ConvNetwork(
                    input_shape=obs_shape,
                    output_dim=1,
                    hidden_sizes=(256, 256, 64),
                    pool_sizes=(2, 2, 2, 2),
                    conv_filters=(32, 64, 64, 64),
                    conv_filter_sizes=(5, 5, 3, 3),
                    conv_strides=(2, 2, 1, 1),
                    conv_pads=(2, 2, 1, 1),
                    hidden_nonlinearity=NL.rectify,
                    output_nonlinearity=NL.tanh,
                )
    
        baseline_std = ConvNetwork(
                    input_shape=obs_shape,
                    input_layer=baseline_network.input_layer,
                    output_dim=1,
                    hidden_sizes=(256, 256, 64),
                    pool_sizes=(2, 2, 2, 2),
                    conv_filters=(32, 64, 64, 64),
                    conv_filter_sizes=(5, 5, 3, 3),
                    conv_strides=(2, 2, 1, 1),
                    conv_pads=(2, 2, 1, 1),
                    hidden_nonlinearity=NL.rectify,
                    output_nonlinearity=NL.tanh,
                )
    
        policy = GaussianMLPPolicy(
                   env_spec=env.spec,
                   mean_network=mean_network,
                   std_network=std_network,
                   #init_std=0.5
                )
    
        reg_args = {
            "mean_network" : baseline_network,
            "std_network" : baseline_std,
            #"adaptive_std" : True,
        }
    
        baseline = GaussianMLPBaseline(
                     env_spec=env.spec,
                     regressor_args=reg_args
                  )
    

    algo = TRPO(
        env=env,
        policy=policy,
        baseline=baseline,
        batch_size=1*env.horizon,
        max_path_length=env.horizon,
        n_itr=4001,
        discount=0.99,
        step_size=0.01,
        # Uncomment both lines (this and the plot parameter below) to enable plotting
        # plot=True,
    )
    algo.train()


run_experiment_lite(
    run_task,
    # Number of parallel workers for sampling
    n_parallel=1,
    # Only keep the snapshot parameters for the last iteration
    snapshot_mode="gap",
    snapshot_gap=50,
)
