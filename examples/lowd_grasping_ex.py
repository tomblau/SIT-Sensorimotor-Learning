from __future__ import print_function
from __future__ import absolute_import

from rllab.misc.instrument import stub, run_experiment_lite

seeds = [975546, 516016, 191858, 728967, 400057, 272061, 166820, 360305, 53825, 243145]
exp_id = 5
seed = seeds[exp_id - 1]


def run_task(*_):
    from rllab.algos.trpo import TRPO
    from rllab.baselines.gaussian_mlp_baseline import GaussianMLPBaseline
    from rllab.envs.vrep.grasping.jaco_env import LowDJacoEnv
    from rllab.envs.normalized_env import normalize
    from rllab.policies.gaussian_mlp_policy import GaussianMLPPolicy
    import lasagne.nonlinearities as NL
    from rllab.core.network import MLP
    from rllab.core.lasagne_helpers import ScaledSigmoidPlus
    import joblib
    
    env = LowDJacoEnv()
    action_dim = env.action_space.flat_dim
    obs_shape = env.observation_space.shape
    init_weights = None
    init_weights="/rllab/data/local/experiment/networks/trained_lowd_grasping%d.pkl" %exp_id
    
    if init_weights:
        data = joblib.load(init_weights)
        policy = data["policy"]
        baseline = data["baseline"]
    else:
        mean_network = MLP(
                         input_shape=obs_shape,
                         output_dim=action_dim,
                         hidden_sizes=(256, 256, 64, 16, 5),
                         hidden_nonlinearity=NL.rectify,
                         output_nonlinearity=NL.identity,
                    )
    
        std_network = MLP(
                        input_shape=obs_shape,
                        input_layer=mean_network.input_layer,
                        output_dim=action_dim,
                        hidden_sizes=(256, 256, 64),
                        hidden_nonlinearity=NL.rectify,
                        output_nonlinearity=ScaledSigmoidPlus(scale_out=-5),
                    )
    
        baseline_network = MLP(
                    input_shape=obs_shape,
                    output_dim=1,
                    hidden_sizes=(256, 256, 64),
                    hidden_nonlinearity=NL.rectify,
                    output_nonlinearity=NL.tanh,
                )
    
        baseline_std = MLP(
                    input_shape=obs_shape,
                    input_layer=baseline_network.input_layer,
                    output_dim=1,
                    hidden_sizes=(256, 256, 64),
                    hidden_nonlinearity=NL.rectify,
                    output_nonlinearity=NL.tanh,
                )
    
        policy = GaussianMLPPolicy(
                   env_spec=env.spec,
                   mean_network=mean_network,
                   std_network=std_network,
                   #init_std=0.5
                )
    
        reg_args = {
            "mean_network" : baseline_network,
            "std_network" : baseline_std,
            #"adaptive_std" : True,
        }
    
        baseline = GaussianMLPBaseline(
                     env_spec=env.spec,
                     regressor_args=reg_args
                  )
    
    
    algo = TRPO(
        env=env,
        policy=policy,
        baseline=baseline,
        batch_size=100,
        max_path_length=25,
        n_itr=10010,
        discount=0.99,
        step_size=0.01,
        # Uncomment both lines (this and the plot parameter below) to enable plotting
        # plot=True,
    )
    algo.train()

run_experiment_lite(
    run_task,
    # Number of parallel workers for sampling
    n_parallel=1,
    seed=seed,
    # Only keep the snapshot parameters for the last iteration
    snapshot_mode="gap",
    snapshot_gap=50,
    # Specifies the seed for the experiment. If this is not provided, a random seed
    # will be used
    # plot=True,
)
